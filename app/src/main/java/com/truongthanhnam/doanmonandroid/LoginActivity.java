package com.truongthanhnam.doanmonandroid;

import android.app.Application;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.truongthanhnam.DAL.ConnectionDAL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class LoginActivity extends AppCompatActivity {

    String dbName = "dbQuanLyBanAn1.sqlite";
    String dbSuf = "/databases/";

    EditText edtNhapUserName,edtNhapPassWord;
    Button btnLogin;
    TextView txtDangKyTaiKhoan;
    TextView txtSuaTaiKhoan;
    TextView txtSettingDatabase;
    ImageView imgHydra;
    public static int phanQuyen=0;
    public static SQLiteDatabase database;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        processCopy();
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        txtSettingDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(LoginActivity.this);
                dialog.setContentView(R.layout.testconnect);
                dialog.show();
                final EditText edtIP = (EditText) dialog.findViewById(R.id.edtIP);
                final EditText edtDatabase = (EditText) dialog.findViewById(R.id.edtDatabase);
                final EditText edtUserName = (EditText) dialog.findViewById(R.id.edtUserName);
                final EditText edtPassword = (EditText) dialog.findViewById(R.id.edtPassword);
                final TextView txtThongBaoKetQua = (TextView) dialog.findViewById(R.id.txtThongBaoKetQua);
                Button btnTest = (Button) dialog.findViewById(R.id.btnTest);
                final Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
                btnSave.setEnabled(false);
                SharedPreferences pre = getSharedPreferences("CauHinh",MODE_PRIVATE);
                edtIP.setText(pre.getString("Ip",""));
                edtDatabase.setText(pre.getString("Database",""));
                edtUserName.setText(pre.getString("UserName",""));
                edtPassword.setText(pre.getString("Password",""));
                btnTest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtIP.getText().toString().isEmpty() || edtDatabase.getText().toString().isEmpty()||edtUserName.getText().toString().isEmpty()||edtPassword.getText().toString().isEmpty())
                        {
                            AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                            b.setTitle("Thông báo");
                            b.setMessage("Bạn chưa nhập thông tin, vui lòng nhập đầy đủ !");
                            b.setIcon(android.R.drawable.ic_dialog_info);
                            b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog d = b.create();
                            d.setCanceledOnTouchOutside(false);
                            d.show();
                        }
                        else
                        {
                           boolean check = ConnectionDAL.isConnect(edtIP.getText().toString().trim(),edtDatabase.getText().toString().trim(),edtUserName.getText().toString().trim(),edtPassword.getText().toString().trim());
                            if (check==true)
                            {
                                txtThongBaoKetQua.setText("Chuỗi kết nối hợp lệ !");
                                btnSave.setEnabled(true);
                            }
                            else
                            {
                                 txtThongBaoKetQua.setText("Chuỗi kêt nối không hợp lệ !");
                            }
                        }
                    }
                });
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences pre = getSharedPreferences("CauHinh",MODE_PRIVATE);
                        SharedPreferences.Editor edi = pre.edit();
                        edi.putString("Ip",edtIP.getText().toString().trim());
                        edi.putString("Database",edtDatabase.getText().toString().trim());
                        edi.putString("UserName",edtUserName.getText().toString().trim());
                        edi.putString("Password",edtPassword.getText().toString().trim());
                        edi.commit();
                        txtThongBaoKetQua.setText("Lưu thành công !");
                        ConnectionDAL.getInformation(edtIP.getText().toString().trim(),edtDatabase.getText().toString().trim(),edtUserName.getText().toString().trim(),edtPassword.getText().toString().trim());
                    }
                });
            }
        });
        txtSuaTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginActivity.this);
                dialog.setContentView(R.layout.suataikhoan);
                dialog.show();
                final EditText edtSuaTen = (EditText) dialog.findViewById(R.id.edtSuaTen);
                final EditText edtPassCu = (EditText) dialog.findViewById(R.id.edtPassCu);
                final EditText edtPassMoi = (EditText) dialog.findViewById(R.id.edtPassMoi);
                Button btnFix = (Button) dialog.findViewById(R.id.btnFix);
                btnFix.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtSuaTen.getText().toString().isEmpty() || edtPassCu.getText().toString().isEmpty() || edtPassMoi.getText().toString().isEmpty())
                        {
                            AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                            b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                            b.setTitle("Thông báo");
                            b.setIcon(android.R.drawable.ic_dialog_info);
                            b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = b.create();
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();
                        }
                        else
                        {
                            boolean kq1 = isExistNameAndPass(edtSuaTen.getText().toString(),edtPassCu.getText().toString());
                            if (kq1)
                            {
                                boolean kq2 = updateAccount(edtSuaTen.getText().toString(),edtPassMoi.getText().toString());
                                if (kq2)
                                {
                                    dialog.dismiss();
                                    Toast.makeText(LoginActivity.this, "Sửa tài khoản thành công", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    dialog.dismiss();
                                    Toast.makeText(LoginActivity.this, "Sửa tài khoản thất bại", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                            {
                                AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                                b.setMessage("Tên hoặc mật khẩu cũ không chính xác,vui lòng nhập lại !");
                                b.setTitle("Thông báo");
                                b.setIcon(android.R.drawable.ic_dialog_info);
                                b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog dialog = b.create();
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.show();
                            }
                        }
                    }
                });
            }
        });
        txtDangKyTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(LoginActivity.this);
                dialog.setContentView(R.layout.dangkytaikhoan);
                dialog.show();
                final EditText edtDangKyTen = (EditText) dialog.findViewById(R.id.edtDangKyTen);
                final EditText edtDangKyPass = (EditText) dialog.findViewById(R.id.edtDangKyMatKhau);
                Button btnUpdate = (Button) dialog.findViewById(R.id.btnEdit);
                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtDangKyTen.getText().toString().isEmpty() || edtDangKyPass.getText().toString().isEmpty())
                        {
                            AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                            b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                            b.setTitle("Thông báo");
                            b.setIcon(android.R.drawable.ic_dialog_info);
                            b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = b.create();
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();
                        }
                        else
                        {
                            boolean kq1 = isExistName(edtDangKyTen.getText().toString());
                            if (kq1)
                            {
                                    AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                                    b.setMessage("Tên này đã tồn tại rồi, vui lòng chọn tên khác !");
                                    b.setTitle("Thông báo");
                                    b.setIcon(android.R.drawable.ic_dialog_info);
                                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog dialog = b.create();
                                    dialog.setCanceledOnTouchOutside(false);
                                    dialog.show();
                            }
                            else
                            {
                                boolean kq2 = insertAccount(edtDangKyTen.getText().toString(),edtDangKyPass.getText().toString());
                                if (kq2)
                                {
                                    dialog.dismiss();
                                    Toast.makeText(LoginActivity.this, "Tạo tài khoản thành công !", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    dialog.dismiss();
                                    Toast.makeText(LoginActivity.this, "Tạo tài khoản thất bại !", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                });
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pre = getSharedPreferences("CauHinh",MODE_PRIVATE);
                String ip = pre.getString("Ip","");
                String database = pre.getString("Database","");
                String username = pre.getString("UserName","");
                String password = pre.getString("Password","");
                if (ip.equals("")||database.equals("")||username.equals("")||password.equals(""))
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Bạn chưa cấu hình kết nối cơ sở dữ liệu, vui lòng thực hiện kết nối !");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog d = b.create();
                    d.setCanceledOnTouchOutside(false);
                    d.show();
                }
                else
                {
                    boolean check = ConnectionDAL.isConnect(ip,database,username,password);
                    if (check==false)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                        b.setMessage("Không thể kết nối, hãy kiểm tra lại đường truyển Internet !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                        return;
                    }
                    ConnectionDAL.getInformation(ip,database,username,password);
                    if (edtNhapUserName.getText().toString().isEmpty() || edtNhapPassWord.getText().toString().isEmpty())
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                        b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        if (edtNhapUserName.getText().toString().equals("giamdoc"))
                        {
                            boolean kq  = isExistNameAndPass(edtNhapUserName.getText().toString(),edtNhapPassWord.getText().toString());
                            if (kq)
                            {
                                Intent intent = new Intent(LoginActivity.this, WelcomeAppActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter, R.anim.exist);
                                phanQuyen = 2;
                            }
                            else
                            {
                                AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                                b.setMessage("Sai tên hoặc mật khẩu, vui lòng nhập lại !");
                                b.setTitle("Thông báo");
                                b.setIcon(android.R.drawable.ic_dialog_info);
                                b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog dialog = b.create();
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.show();
                            }
                        }
                        else if (edtNhapUserName.getText().toString().equals("phache"))
                        {
                            boolean kq  = isExistNameAndPass(edtNhapUserName.getText().toString(),edtNhapPassWord.getText().toString());
                            if (kq)
                            {
                                Intent intent = new Intent(LoginActivity.this, WelcomeAppActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter, R.anim.exist);
                                phanQuyen = 1;
                            }
                            else
                            {
                                AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                                b.setMessage("Sai tên hoặc mật khẩu, vui lòng nhập lại !");
                                b.setTitle("Thông báo");
                                b.setIcon(android.R.drawable.ic_dialog_info);
                                b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog dialog = b.create();
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.show();
                            }
                        }
                        else
                        {
                            boolean kq = isExistNameAndPass(edtNhapUserName.getText().toString(), edtNhapPassWord.getText().toString());
                            if (kq)
                            {
                                Intent intent = new Intent(LoginActivity.this, WelcomeAppActivity.class);
                                startActivity(intent);
                                phanQuyen=0;
                            } else
                            {
                                AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
                                b.setMessage("Sai tên hoặc mật khẩu, vui lòng nhập lại !");
                                b.setTitle("Thông báo");
                                b.setIcon(android.R.drawable.ic_dialog_info);
                                b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog dialog = b.create();
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.show();
                            }
                        }
                    }
                }
            }
        });
    }
    private boolean isExistName(String name)
    {
        Cursor c = database.query("Account",null,"name=?",new String[]{name},null,null,null);
        while (c.moveToNext())
        {
            if (c.getString(0).equals(name))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    private boolean isExistNameAndPass(String name,String pass)
    {
        Cursor c = database.query("Account",null,"name=? and passWord=?",new String[]{name,pass},null,null,null);
        while (c.moveToNext())
        {
            if (c.getString(0).equals(name) && c.getString(1).equals(pass))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    private boolean insertAccount(String name,String pass)
    {
        ContentValues value = new ContentValues();
        value.put("name",name);
        value.put("passWord",pass);
        long kq = database.insert("Account",null,value);
        if (kq>0)
            return true;
        return false;
    }
    private boolean updateAccount(String name,String pass)
    {
        ContentValues value = new ContentValues();
        value.put("passWord",pass);
        int kq = database.update("Account",value,"name=?",new String[]{name});
        if (kq>0)
            return true;
         return false;
    }

    private void addControls()
    {
        edtNhapUserName = (EditText) findViewById(R.id.edtNhapUserName);
        edtNhapPassWord = (EditText) findViewById(R.id.edtNhapPassWord);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtDangKyTaiKhoan = (TextView) findViewById(R.id.txtDangKyTaiKhoan);
        txtSuaTaiKhoan = (TextView) findViewById(R.id.txtSuaTaiKhoan);
        imgHydra = (ImageView) findViewById(R.id.imgHydra);
        txtSettingDatabase = (TextView) findViewById(R.id.txtSettingDatabase);
        database = openOrCreateDatabase(dbName,MODE_PRIVATE,null);
    }

    private void processCopy()
    {
        try
        {
            File f = getDatabasePath(dbName);
            if (!f.exists())
            {
                copyFromAsset();
                Toast.makeText(this, "Đã có database", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception ex)
        {
            Log.e("Error : ",ex.toString());
        }
    }

    private void copyFromAsset()
    {
        try
        {
            InputStream myInput = getAssets().open(dbName);
            String path = getApplicationInfo().dataDir+dbSuf+dbName;
            File f = new File(getApplicationInfo().dataDir+dbSuf);
            if (!f.exists())
                f.mkdir();
            OutputStream myOutput = new FileOutputStream(path);
            byte[]buffer = new byte[1024];
            int length;
            while((length=myInput.read(buffer))>0)
                myOutput.write(buffer,0,length);
            myOutput.flush();
            myOutput.close();
            myInput.close();
        }
        catch (Exception ex)
        {
            Log.e("Error : ",ex.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Animation a = AnimationUtils.loadAnimation(LoginActivity.this,R.anim.shrink);
        imgHydra.setAnimation(a);
        SharedPreferences pre = getSharedPreferences("TrangThai",MODE_PRIVATE);
        edtNhapUserName.setText(pre.getString("UserName",""));
        edtNhapPassWord.setText(pre.getString("PassWord",""));
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pre = getSharedPreferences("TrangThai",MODE_PRIVATE);
        SharedPreferences.Editor edi = pre.edit();
        edi.putString("UserName",edtNhapUserName.getText().toString());
        edi.putString("PassWord",edtNhapPassWord.getText().toString());
        edi.commit();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this);
        b.setTitle("Hỏi thoát");
        b.setMessage("Bạn có chắc muốn thoát chương trình");
        b.setIcon(android.R.drawable.ic_dialog_info);
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        b.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = b.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
