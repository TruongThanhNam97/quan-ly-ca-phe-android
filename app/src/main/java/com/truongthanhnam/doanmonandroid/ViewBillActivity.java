package com.truongthanhnam.doanmonandroid;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.truongthanhnam.adapter.BillAdapter;
import com.truongthanhnam.model.Bill;
import com.truongthanhnam.model.Table;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class ViewBillActivity extends AppCompatActivity {

    Spinner spinnerTableToSearchBill;
    ArrayAdapter<Table> adapterTable;
    ListView lvAllBill;
    BillAdapter adapterBill;
    TextView txtTotalIncome;
    Table selectedTable;
    public static Bill selectedBill;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bill);
        addControls();
        addTableForSpinner();
        addEvents();
    }

    private void addEvents()
    {
        spinnerTableToSearchBill.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedTable=adapterTable.getItem(position);
                addBillForListView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lvAllBill.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedBill = adapterBill.getItem(position);
                return false;
            }
        });
    }

    private void addControls() {
        spinnerTableToSearchBill = (Spinner) findViewById(R.id.spinnerTableToSearchBill);
        adapterTable = new ArrayAdapter<Table>(ViewBillActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterTable.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerTableToSearchBill.setAdapter(adapterTable);
        lvAllBill = (ListView) findViewById(R.id.lvAllBill);
        adapterBill = new BillAdapter(ViewBillActivity.this,R.layout.itembill);
        lvAllBill.setAdapter(adapterBill);
        txtTotalIncome= (TextView) findViewById(R.id.txtTotalIncome);
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtTotalIncome.setText(df.format(getTotalIncome())+" VNĐ");
        registerForContextMenu(lvAllBill);
    }
    private void addBillForListView()
    {
        Cursor c = LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{selectedTable.getIdTable()+"",1+""},null,null,null);
        adapterBill.clear();
        while (c.moveToNext())
            adapterBill.add(new Bill(c.getInt(0),c.getInt(1),c.getInt(2),c.getDouble(3)));
        c.close();
    }

    private void addTableForSpinner()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        adapterTable.clear();
        while (c.moveToNext())
            adapterTable.add(new Table(c.getInt(0),c.getString(1),c.getString(2)));
        c.close();
    }
    private double getTotalIncome()
    {
        double totalIncome=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Bill where statusBill=1",null);
        while (c.moveToNext())
            totalIncome+=c.getDouble(3);
        return totalIncome;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextbill,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuViewInformationBill)
        {
            Intent intent = new Intent(ViewBillActivity.this,ViewInformationBillActivity.class);
            startActivity(intent);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionbillinfo,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearchPay);
        SearchView searchView = (SearchView) mnu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    addBillForListView();
                }
                else
                {
                    adapterBill.clear();
                    Cursor c = LoginActivity.database.query("Bill",null,"statusBill=?",new String[]{1+""},null,null,null);
                    while (c.moveToNext())
                    {
                        if ((c.getDouble(3)+"").contains(newText))
                            adapterBill.add(new Bill(c.getInt(0),c.getInt(1),c.getInt(2),c.getDouble(3)));
                    }

                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
