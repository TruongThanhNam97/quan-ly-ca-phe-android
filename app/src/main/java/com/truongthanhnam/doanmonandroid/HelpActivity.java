package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

public class HelpActivity extends AppCompatActivity {

    TextView txtClearAllData;
    TextView txtThongKe;
    TextView txtQuanLyNhanVien;
    TextView txtTraCuuHoaDon,txtTraCuuMonAn,txtTraCuuMucMonAn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        txtClearAllData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(HelpActivity.this);
                b.setTitle("Chẩn đoán");
                b.setMessage("Sử dụng một thời gian dài do dữ liệu lưu quá nhiều có thể khiến chương trình bị chậm đi, để khắc phục bạn có thể xóa toàn bộ dữ liệu");
                b.setIcon(android.R.drawable.ic_dialog_info);
                b.setPositiveButton("Xóa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LoginActivity.database.delete("BillInfo","",new String[]{});
                        updateAllStatusTable("Có người");
                        Toast.makeText(HelpActivity.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                    }
                });
                b.setNegativeButton("Không xóa", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = b.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
        txtThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this,StatisticalActivity.class);
                startActivity(intent);
            }
        });
        txtQuanLyNhanVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this,QuanLyNhanVienActivity.class);
                startActivity(intent);
            }
        });
        txtTraCuuHoaDon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this,ViewBillActivity.class);
                startActivity(intent);
            }
        });
        txtTraCuuMonAn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this,ViewFoodActivity.class);
                startActivity(intent);
            }
        });
        txtTraCuuMucMonAn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HelpActivity.this,ViewFoodCateGoryMainActivity.class);
                startActivity(intent);
            }
        });
    }
    private void addControls()
    {
        txtClearAllData = (TextView) findViewById(R.id.txtClearAllData);
        txtThongKe = (TextView) findViewById(R.id.txtThongKe);
        txtQuanLyNhanVien = (TextView) findViewById(R.id.txtQuanLyNhanVien);
        txtTraCuuHoaDon = (TextView) findViewById(R.id.txtTraCuuHoaDon);
        txtTraCuuMonAn = (TextView) findViewById(R.id.txtTraCuuMonAn);
        txtTraCuuMucMonAn = (TextView) findViewById(R.id.txtTraCuuMucMonAn);
    }
    private void updateAllStatusTable(String str)
    {
        Cursor c = LoginActivity.database.query("Ban",null,"statusTable=?",new String[]{str},null,null,null);
        while (c.moveToNext())
        {
            ContentValues value = new ContentValues();
            value.put("statusTable","Trống");
            LoginActivity.database.update("Ban",value,"idTable=?",new String[]{c.getInt(0)+""});
        }
        c.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }
}
