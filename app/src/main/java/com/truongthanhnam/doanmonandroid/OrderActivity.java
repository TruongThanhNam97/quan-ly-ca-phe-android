package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.adapter.OrderFoodAdapter;
import com.truongthanhnam.model.Food;
import com.truongthanhnam.model.FoodCateGory;
import com.truongthanhnam.model.Table;

import java.util.ArrayList;

public class OrderActivity extends AppCompatActivity {

    Spinner spinnerFoodCateGoryOrder;
    ArrayAdapter<FoodCateGory> adapterFoodCateGory;
    ListView lvFoodOrder;
    OrderFoodAdapter adapterFood;
    int x=0;
    FoodCateGory selectedFoodCateGory;
    public static TextView txtCountFood,txtDisplayCountFood;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        addControls();
        addEvents();
    }

    private void addEvents() {
        spinnerFoodCateGoryOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFoodCateGory = adapterFoodCateGory.getItem(position);
                addFoodByFoodCateGoryToListView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void addFoodByFoodCateGoryToListView()
    {
        adapterFood.clear();
        Cursor c = LoginActivity.database.query("Food",null,"idFoodCateGory=?",new String[]{selectedFoodCateGory.getIdFoodCateGory()+""},null,null,null);
        while (c.moveToNext())
            adapterFood.add(new Food(c.getInt(0),c.getString(1),c.getInt(2),c.getDouble(3)));
        c.close();
    }

    private void addControls() {
        spinnerFoodCateGoryOrder = (Spinner) findViewById(R.id.spinnerFoodCateGoryOrder);
        adapterFoodCateGory = new ArrayAdapter<FoodCateGory>(OrderActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterFoodCateGory.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerFoodCateGoryOrder.setAdapter(adapterFoodCateGory);
        lvFoodOrder = (ListView) findViewById(R.id.lvFoodOrDer);
        adapterFood = new OrderFoodAdapter(OrderActivity.this,R.layout.itemfoodorder);
        adapterFood = new OrderFoodAdapter(OrderActivity.this,R.layout.itemfoodorder);
        lvFoodOrder.setAdapter(adapterFood);
        txtCountFood = (TextView) findViewById(R.id.txtCountFood);
        txtCountFood.setText(1+"");
        txtDisplayCountFood = (TextView) findViewById(R.id.txtDisplayCountFood);
        addFoodCateGoryToSpinner();
    }

    private void addFoodCateGoryToSpinner()
    {
        adapterFoodCateGory.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        while (c.moveToNext())
            adapterFoodCateGory.add(new FoodCateGory(c.getInt(0),c.getString(1)));
        c.close();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ContentValues value = new ContentValues();
        value.put("totalPrice",getTotalPriceOfBill());
        LoginActivity.database.update("Bill",value,"idBill=?",new String[]{getIdBillByIdTableAndStatusBill()+""});
        OrderDAL.updateStatusOrderByIdOrder(getIdBillByIdTableAndStatusBill(),0);
    }
    private double getPriceByIdFood(int id)
    {
        double price=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            price=c.getDouble(3);
        c.close();
        return price;
    }
    private int getIdBillByIdTableAndStatusBill()
    {
        int id=0;
        Cursor c =  LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{MenuActivity.selectedTable.getIdTable()+"",0+""},null,null,null);
        while (c.moveToNext())
            id=c.getInt(0);
        c.close();
        return id;
    }
    private double getTotalPriceOfBill()
    {
        double sum=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+getIdBillByIdTableAndStatusBill()+"",null);
        while (c.moveToNext())
            sum+=c.getInt(3)*1.0*getPriceByIdFood(c.getInt(2));
        c.close();
        return sum;
    }
}
