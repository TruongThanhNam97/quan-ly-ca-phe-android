package com.truongthanhnam.doanmonandroid;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.truongthanhnam.adapter.MyInfoAdapter;
import com.truongthanhnam.model.House;

public class GoogleMapActivity extends AppCompatActivity {

    private GoogleMap mMap;
    Spinner spinnerMapType;
    ArrayAdapter<String> adapter;
    String[] arr = {"Normal", "Satellite", "Terrain", "Hybrid", "None"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                LatLng pos = new LatLng(HouseActivity.seletedHouse.getLatite(),HouseActivity.seletedHouse.getLongtite());
                Marker marker = mMap.addMarker(new MarkerOptions().position(pos));
                MyInfoAdapter myInfoAdapter = new MyInfoAdapter(GoogleMapActivity.this,HouseActivity.seletedHouse);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos,16));
                mMap.setInfoWindowAdapter(myInfoAdapter);
                marker.showInfoWindow();
            }
        });
        addControls();
        addEvents();
    }

    private void addEvents() {
        spinnerMapType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                xuLy(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void xuLy(int position) {
        switch (position)
        {
            case 0:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case 1:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case 2:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case 3:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case 4:
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
        }
    }

    private void addControls() {
        spinnerMapType = (Spinner) findViewById(R.id.spinnerMapType);
        adapter = new ArrayAdapter<String>(GoogleMapActivity.this,android.R.layout.simple_spinner_dropdown_item,arr);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerMapType.setAdapter(adapter);
    }
}
