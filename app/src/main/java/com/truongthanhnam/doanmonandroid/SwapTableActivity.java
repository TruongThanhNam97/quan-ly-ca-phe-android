package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DAL.OrderInfoDAL;
import com.truongthanhnam.DTO.OrderInfo;
import com.truongthanhnam.model.BillInfo;
import com.truongthanhnam.model.Table;

import java.util.ArrayList;

public class SwapTableActivity extends AppCompatActivity {

    EditText edtTableFrom;
    Spinner spinnerTableTo;
    ArrayAdapter<Table> adapter;
    Button btnSwap;
    Table selectedTable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swap_table);
        addControls();
        addTableToSpinner();
        addEvents();
    }

    private void addEvents()
    {
        spinnerTableTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedTable = adapter.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MenuActivity.selectedTable.getStatusTable().equals("Trống"))
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(SwapTableActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage(MenuActivity.selectedTable.getNameTable()+" hiện đang trống !");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          finish();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    if (selectedTable.getStatusTable().equals("Trống"))
                    {
                        updateIdTableSQL();
                        updateStatusTableFrom();
                        updateStatusTableTo();
                        updateIdTableOfBillTableFrom();
                        Toast.makeText(SwapTableActivity.this, "Chuyển bàn thành công !", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else if (selectedTable.getStatusTable().equals("Có người"))
                    {
                       // updateList();
                        updateStatusTableFrom();
                        updateListBillInfoFromBillOfTablefromToBillOfTableto();
                        deleteBillOfTableFrom();
                        updateToTalPriceOfBillOfTableTo();
                        Toast.makeText(SwapTableActivity.this, "Gộp bàn thành công !", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        });
    }

    private void updateList()
    {
        ArrayList<OrderInfo> listOrderInfo = OrderInfoDAL.getListOrderInfoByIdOrder(getIdBillFromTableFrom());
        for (final OrderInfo x : listOrderInfo)
        {
            int z=0;
            ArrayList<OrderInfo> listOrderInfo2 = OrderInfoDAL.getListOrderInfoByIdOrder(getIdBillFromTableTo());
            for (final OrderInfo y : listOrderInfo2)
            {
                if (x.getIdFoodd()==y.getIdFoodd())
                {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            OrderInfoDAL.updateCountOrderInfoInOrderInfo(y.getIdOrderInfo(),x.getCountOrderInfo()+y.getCountOrderInfo());
                            OrderInfoDAL.deleteOrderInfo(x.getIdOrderInfo());
                        }
                    });
                    t.start();
                    z++;
                }
            }
            if (z==0)
            {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        OrderInfoDAL.updateIdOrder(x.getIdOrder(),getIdBillFromTableTo());
                    }
                });
                t.start();
            }
        }
        OrderDAL.deleteOrder(getIdBillFromTableFrom());
    }

    private void updateIdTableSQL()
    {
        OrderDAL.updateIdTable(getIdBillFromTableFrom(),selectedTable.getIdTable());
    }

    private void addTableToSpinner()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        adapter.clear();
        while (c.moveToNext())
        {
            if (!c.getString(1).equals(MenuActivity.selectedTable.getNameTable()))
            {
                adapter.add(new Table(c.getInt(0),c.getString(1),c.getString(2)));
            }
        }
        c.close();
    }

    private void addControls()
    {
        edtTableFrom = (EditText) findViewById(R.id.edtTableFrom);
        spinnerTableTo = (Spinner) findViewById(R.id.spinnerTableTo);
        adapter = new ArrayAdapter<Table>(SwapTableActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerTableTo.setAdapter(adapter);
        btnSwap = (Button) findViewById(R.id.btnSwap);
        edtTableFrom.setText(MenuActivity.selectedTable.getNameTable());
        edtTableFrom.setEnabled(false);
    }
    private int getIdBillFromTableFrom()
    {
        int id=0;
        Cursor c = LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{MenuActivity.selectedTable.getIdTable()+"",0+""},null,null,null);
        while (c.moveToNext())
            id=c.getInt(0);
        return id;
    }
    private void updateStatusTableTo()
    {
        ContentValues value = new ContentValues();
        value.put("statusTable","Có người");
        LoginActivity.database.update("Ban",value,"idTable=?",new String[]{selectedTable.getIdTable()+""});
    }
    private void updateStatusTableFrom()
    {
        ContentValues value = new ContentValues();
        value.put("statusTable","Trống");
        LoginActivity.database.update("Ban",value,"idTable=?",new String[]{MenuActivity.selectedTable.getIdTable()+""});
    }
    private void updateIdTableOfBillTableFrom()
    {
        ContentValues value = new ContentValues();
        value.put("idTable",selectedTable.getIdTable());
        LoginActivity.database.update("Bill",value,"idBill=?",new String[]{getIdBillFromTableFrom()+""});
    }
    private int getIdBillFromTableTo()
    {
        int id=0;
        Cursor c = LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{selectedTable.getIdTable()+"",0+""},null,null,null);
        while (c.moveToNext())
            id=c.getInt(0);
        return id;
    }
    private void deleteBillOfTableFrom()
    {

        int id = getIdBillFromTableFrom();
        OrderDAL.deleteOrder(id);
        LoginActivity.database.delete("Bill","idBill=?",new String[]{getIdBillFromTableFrom()+""});
    }
    private  void updateListBillInfoFromBillOfTablefromToBillOfTableto()
    {
        final Cursor c = LoginActivity.database.query("BillInfo",null,"idBill=?",new String[]{getIdBillFromTableFrom()+""},null,null,null);
        while (c.moveToNext())
        {
            Cursor cu = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+getIdBillFromTableTo()+"",null);
            int z=0;
            while (cu.moveToNext())
            {
                if (cu.getInt(2)==c.getInt(2))
                {
                     int idbillinfoOfTableTo = cu.getInt(0);
                     int idbillInfoOfTableFrom = c.getInt(0);
                     int countFrom = c.getInt(3);
                     int countTo = cu.getInt(3);
                    OrderInfoDAL.updateCountOrderInfoInOrderInfo(OrderInfoDAL.getIdBillInfoByIdOrderAndIdFood(cu.getInt(2),cu.getInt(1)),OrderInfoDAL.getCountOrderInfoByIdOrderAmdIdFood(c.getInt(2),c.getInt(1))+OrderInfoDAL.getCountOrderInfoByIdOrderAmdIdFood(cu.getInt(2),cu.getInt(1)));
                    OrderInfoDAL.deleteOrderInfo(OrderInfoDAL.getIdBillInfoByIdOrderAndIdFood(c.getInt(2),c.getInt(1)));
                    ContentValues value = new ContentValues();
                    value.put("count",c.getInt(3)+cu.getInt(3));
                    LoginActivity.database.update("BillInfo",value,"idBillInfo=?",new String[]{cu.getInt(0)+""});
                    LoginActivity.database.delete("BillInfo","idBillInfo=?",new String[]{c.getInt(0)+""});
                    z++;
                }
            }
            if (z==0)
            {
                OrderInfoDAL.updateIdOrder(OrderInfoDAL.getIdBillInfoByIdOrderAndIdFood(c.getInt(2),c.getInt(1)),getIdBillFromTableTo());
                ContentValues value = new ContentValues();
                value.put("idBill",getIdBillFromTableTo());
                LoginActivity.database.update("BillInfo",value,"idBillInfo=?",new String[]{c.getInt(0)+""});
            }
        }
        c.close();
    }
    private double getTotalPrice()
    {
        double totalPrice=0.0;
        Cursor c = LoginActivity.database.query("BillInfo",null,"idBill=?",new String[]{getIdBillFromTableTo()+""},null,null,null);
        while (c.moveToNext())
            totalPrice+=getPriceByIdFood(c.getInt(2))*c.getInt(3)*1.0;
        return totalPrice;
    }
    private double getPriceByIdFood(int id)
    {
        double price=0.0;
        Cursor c= LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            price=c.getDouble(3);
        return price;
    }
    private void updateToTalPriceOfBillOfTableTo()
    {
        ContentValues value = new ContentValues();
        value.put("totalPrice",getTotalPrice());
        LoginActivity.database.update("Bill",value,"idBill=?",new String[]{getIdBillFromTableTo()+""});
    }
}
