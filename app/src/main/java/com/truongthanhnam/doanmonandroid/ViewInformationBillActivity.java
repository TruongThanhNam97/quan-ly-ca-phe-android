package com.truongthanhnam.doanmonandroid;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SearchView;

import com.truongthanhnam.adapter.BillInfoAdapter;
import com.truongthanhnam.model.BillInfo;

public class ViewInformationBillActivity extends AppCompatActivity {

    ListView lvInformation;
    BillInfoAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_information_bill);
        addControls();
        addBillInfoForListView();
    }

    private void addBillInfoForListView()
    {
        adapter.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+ViewBillActivity.selectedBill.getIdBill()+"",null);
        while (c.moveToNext())
            adapter.add(new BillInfo(c.getInt(0),c.getInt(1),c.getInt(2),c.getInt(3)));
        c.close();
    }

    private void addControls() {
        lvInformation = (ListView) findViewById(R.id.lvInformation);
        adapter = new BillInfoAdapter(ViewInformationBillActivity.this,R.layout.itembillinfo);
        lvInformation.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionbillinfo,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearchPay);
        SearchView searchView = (SearchView) mnu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    addBillInfoForListView();
                }
                else
                {
                    adapter.clear();
                    Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+ViewBillActivity.selectedBill.getIdBill()+"",null);
                    while (c.moveToNext())
                    {
                        if (getNameFoodByIdFood(c.getInt(2)).toLowerCase().contains(newText.toLowerCase()) || (getPriceFoodByIdFood(c.getInt(2))+"").contains(newText))
                            adapter.add(new BillInfo(c.getInt(0),c.getInt(1),c.getInt(2),c.getInt(3)));
                    }
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    private  String getNameFoodByIdFood(int id)
    {
        String str = "";
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            str = c.getString(1);
        return str;
    }
    private double getPriceFoodByIdFood(int id)
    {
        double price=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            price=c.getDouble(3);
        return price;
    }
}
