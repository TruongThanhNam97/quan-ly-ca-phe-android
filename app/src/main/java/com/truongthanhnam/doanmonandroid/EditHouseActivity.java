package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.truongthanhnam.DAL.ConvertDAL;

public class EditHouseActivity extends AppCompatActivity {

    ImageView imgEditHouse;
    EditText edtEditIdHouse,edtEditNameHouse,edtEditViTuyen,edtEditKinhTuyen;
    Button btnEditHouse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_house);
        addControls();
        addEvents();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1 && resultCode==RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imgEditHouse.setImageBitmap(photo);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addEvents() {
        imgEditHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,1);
            }
        });
        btnEditHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEditIdHouse.getText().toString().isEmpty()||edtEditNameHouse.getText().toString().isEmpty()||edtEditViTuyen.getText().toString().isEmpty()||edtEditKinhTuyen.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(EditHouseActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Bạn chưa nhập đủ thông tin, vui lòng nhập đầy đủ !");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    if (edtEditNameHouse.getText().toString().toLowerCase().equals(HouseActivity.seletedHouse.getNameHouse().toLowerCase()))
                    {
                        BitmapDrawable b = (BitmapDrawable) imgEditHouse.getDrawable();
                        editHouse(Integer.parseInt(edtEditIdHouse.getText().toString()),edtEditNameHouse.getText().toString(),Float.parseFloat(edtEditViTuyen.getText().toString()),Float.parseFloat(edtEditKinhTuyen.getText().toString()),b);
                        Toast.makeText(EditHouseActivity.this, "Edit thành công !", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        boolean kq = isExistNameHouse(edtEditNameHouse.getText().toString().trim());
                        if (kq) {
                            AlertDialog.Builder b = new AlertDialog.Builder(EditHouseActivity.this);
                            b.setTitle("Thông báo");
                            b.setMessage("Tên nhà hàng đã tồn tại, vui lòng nhập lại !");
                            b.setIcon(android.R.drawable.ic_dialog_info);
                            b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = b.create();
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();
                        } else {
                            BitmapDrawable b = (BitmapDrawable) imgEditHouse.getDrawable();
                            editHouse(Integer.parseInt(edtEditIdHouse.getText().toString()), edtEditNameHouse.getText().toString(), Float.parseFloat(edtEditViTuyen.getText().toString()), Float.parseFloat(edtEditKinhTuyen.getText().toString()), b);
                            Toast.makeText(EditHouseActivity.this, "Edit thành công !", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
            }
        });
    }

    private void addControls() {
        imgEditHouse = (ImageView) findViewById(R.id.imgEditHouse);
        edtEditIdHouse = (EditText) findViewById(R.id.edtEditIdHouse);
        edtEditNameHouse = (EditText) findViewById(R.id.edtEditNameHouse);
        edtEditViTuyen = (EditText) findViewById(R.id.edtEditViTuyen);
        edtEditKinhTuyen = (EditText) findViewById(R.id.edtEditKinhTuyen);
        btnEditHouse = (Button) findViewById(R.id.btnEditHouse);
        edtEditIdHouse.setText(HouseActivity.seletedHouse.getIdHouse()+"");
        edtEditIdHouse.setEnabled(false);
        edtEditNameHouse.setText(HouseActivity.seletedHouse.getNameHouse());
        edtEditViTuyen.setText(HouseActivity.seletedHouse.getLatite()+"");
        edtEditKinhTuyen.setText(HouseActivity.seletedHouse.getLongtite()+"");
        imgEditHouse.setImageBitmap(ConvertDAL.byteArrayToBitMap(HouseActivity.seletedHouse.getAvatarHouse()));
    }
    private boolean isExistNameHouse(String nameHouse)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from House",null);
        while (c.moveToNext())
        {
            if (nameHouse.toLowerCase().trim().equals(c.getString(1).toLowerCase().trim()))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    private void editHouse(int idHouse, String nameHouse, float latite, float longtite, BitmapDrawable b)
    {
        ContentValues value = new ContentValues();
        value.put("nameHouse",nameHouse);
        value.put("latite",latite);
        value.put("longtite",longtite);
        value.put("avatarHouse",ConvertDAL.imageToByteArray(b));
        LoginActivity.database.update("House",value,"idHouse=?",new String[]{idHouse+""});
    }
}
