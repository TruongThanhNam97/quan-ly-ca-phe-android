package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.truongthanhnam.model.Food;

import java.util.ArrayList;

public class ChartActivity extends AppCompatActivity {

    TabHost tabHost;
    Button btnView1,btnView2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        btnView1.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(ChartActivity.this,BieuDoCotFoodActivity.class);
              startActivity(intent);
          }
      });
        btnView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChartActivity.this,BieuDoCotTableActivity.class);
                startActivity(intent);
            }
        });
    }
    private void addControls() {
        tabHost = (TabHost) findViewById(R.id.tabHostChart);
        tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("tab4");
        tab1.setContent(R.id.tab4);
        tab1.setIndicator("",getResources().getDrawable(R.drawable.food));
        tabHost.addTab(tab1);

        TabHost.TabSpec tab2 = tabHost.newTabSpec("tab5");
        tab2.setContent(R.id.tab5);
        tab2.setIndicator("",getResources().getDrawable(R.drawable.table2));
        tabHost.addTab(tab2);

        btnView1 = (Button) findViewById(R.id.btnView1);
        btnView2 = (Button) findViewById(R.id.btnView2);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }
}
