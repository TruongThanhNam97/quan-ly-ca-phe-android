package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.truongthanhnam.DAL.FooddDAL;
import com.truongthanhnam.model.FoodCateGory;

public class AddFoodActivity extends AppCompatActivity {

    Spinner spinnerFoodCateGory;
    ArrayAdapter<FoodCateGory> adapter;
    EditText edtIdFood,edtNameFood,edtPriceFood;
    Button btnAddFood;
    FoodCateGory selectedFoodCateGory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        spinnerFoodCateGory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFoodCateGory = adapter.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnAddFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIdFood.getText().toString().isEmpty()||edtNameFood.getText().toString().isEmpty()||edtPriceFood.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(AddFoodActivity.this);
                    b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                    b.setTitle("Thông báo");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq1 = isCheckExistNameFood(edtNameFood.getText().toString());
                    if (kq1)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(AddFoodActivity.this);
                        b.setMessage("Tên món ăn này tồn tại rồi, vui lòng nhập lại !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        boolean kq2 = addFood(Integer.parseInt(edtIdFood.getText().toString()),edtNameFood.getText().toString(),selectedFoodCateGory.getIdFoodCateGory(),Double.parseDouble(edtPriceFood.getText().toString()));
                        if (kq2)
                        {
                            FooddDAL.addFoodd(Integer.parseInt(edtIdFood.getText().toString()),edtNameFood.getText().toString());
                            Toast.makeText(AddFoodActivity.this, "Thêm món ăn thành công !", Toast.LENGTH_SHORT).show();
                            edtIdFood.setText(autoIdFood()+"");
                            edtNameFood.setText("");
                            edtPriceFood.setText("");
                            edtNameFood.requestFocus();
                        }
                        else
                        {
                            Toast.makeText(AddFoodActivity.this, "Thêm món ăn thất bại !", Toast.LENGTH_SHORT).show();
                            edtIdFood.setText(autoIdFood()+"");
                            edtNameFood.setText("");
                            edtPriceFood.setText("");
                            edtNameFood.requestFocus();
                        }
                    }
                }
            }
        });
    }

    private void addControls()
    {
        spinnerFoodCateGory = (Spinner) findViewById(R.id.spinnerFoodCateGory);
        adapter = new ArrayAdapter<FoodCateGory>(AddFoodActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerFoodCateGory.setAdapter(adapter);
        addFoodCateGoryToSpinner();
        edtIdFood = (EditText) findViewById(R.id.edtIdFood);
        edtNameFood = (EditText) findViewById(R.id.edtNameFood);
        edtPriceFood = (EditText) findViewById(R.id.edtPriceFood);
        btnAddFood = (Button) findViewById(R.id.btnAddFood);
        edtIdFood.setEnabled(false);
        edtIdFood.setText(autoIdFood()+"");
        edtNameFood.requestFocus();
    }

    private void addFoodCateGoryToSpinner() {
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        adapter.clear();
        while (c.moveToNext())
            adapter.add(new FoodCateGory(c.getInt(0),c.getString(1)));
        c.close();
    }
    private int autoIdFood()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food",null);
        while (c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }
    private boolean isCheckExistNameFood(String nameFood)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Food",null);
        while (c.moveToNext())
        {
            if (c.getString(1).toLowerCase().equals(nameFood.toLowerCase()))
                return true;
        }
        return false;
    }
    private boolean addFood(int idFood,String nameFood,int idFoodCateGory,double price)
    {
        ContentValues value = new ContentValues();
        value.put("idFood",idFood);
        value.put("nameFood",nameFood);
        value.put("idFoodCateGory",idFoodCateGory);
        value.put("price",price);
        long kq = LoginActivity.database.insert("Food",null,value);
        if (kq>0)
            return true;
        return false;
    }
}
