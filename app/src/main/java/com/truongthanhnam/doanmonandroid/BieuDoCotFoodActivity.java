package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class BieuDoCotFoodActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bieu_do_cot_food);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoginActivity.database.delete("ThongKeFood",null,null);
        addListFood();
        displayChartFood();
    }

    private void displayChartFood()
    {
        ArrayList<BarEntry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeFood order by count DESC",null);
        int i=-1;
        while (c.moveToNext())
        {
            i++;
            entries.add(new BarEntry(c.getInt(2),i)); //đối số thứ nhất là giá trị , đối số thứ hai là vị trí
            labels.add(getNameFoodByIdFood(c.getInt(0))); //thiết lập trục hoành
        }
        c.close();

        BarDataSet dataset = new BarDataSet(entries,"Món ăn"); //nguồn dữ liệu cho biểu đồ
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData data = new BarData(labels,dataset); //tạo dữ liệu cho biểu đồ

        BarChart chart = new BarChart(BieuDoCotFoodActivity.this);  //tạo biểu đồ
        chart.setDescription("Biểu đồ thống kê số lần được gọi của món ăn");
        chart.animateY(3000);
        chart.setData(data);
        setContentView(chart);

    }
    private String getNameFoodByIdFood(int idFood)
    {
        String str="";
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood=?",new String[]{idFood+""});
        while (c.moveToNext())
            str = c.getString(1);
        c.close();
        return str;
    }
    private void addListFood()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Food",null);
        while (c.moveToNext())
        {
            int count =0;
            Cursor cu = LoginActivity.database.rawQuery("select * from BillInfo",null);
            while (cu.moveToNext())
            {
                if (cu.getInt(2)==c.getInt(0))
                    count++;
            }
            cu.close();
            ContentValues value = new ContentValues();
            value.put("idThongKeFood",autoIdThongKeFood());
            value.put("idFood",c.getInt(0));
            value.put("count",count);
            LoginActivity.database.insert("ThongKeFood",null,value);
        }
        c.close();
    }
    private int autoIdThongKeFood()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeFood",null);
        while (c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }
}
