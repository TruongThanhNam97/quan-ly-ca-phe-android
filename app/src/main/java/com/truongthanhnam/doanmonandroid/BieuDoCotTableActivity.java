package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class BieuDoCotTableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bieu_do_cot_table);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoginActivity.database.delete("ThongKeTable",null,null);
        addListTable();
        displayChartTable();
    }

    private void displayChartTable()
    {
        ArrayList<BarEntry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();

        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeTable order by count DESC",null);
        int i=-1;
        while (c.moveToNext())
        {
            i++;
            entries.add(new BarEntry(c.getInt(2),i)); //đối số thứ nhất là giá trị , đối số thứ 2 là vị trí
            labels.add(getNameTableByIdTable(c.getInt(0))); //thiết lập trục hoành
        }
        c.close();

        BarDataSet dataset = new BarDataSet(entries,"Bàn ăn"); //nguồn dữ liệu của biểu đồ
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData data = new BarData(labels,dataset); //tạo dữ liệu cho biểu đồ

        BarChart chart = new BarChart(BieuDoCotTableActivity.this); //tạo biểu đồ
        chart.setDescription("Biểu đồ thống kê tổng số bill của bàn ăn");
        chart.animateY(3000);
        chart.setData(data);
        setContentView(chart);
    }
    private String getNameTableByIdTable(int idTable)
    {
        String str="";
        Cursor c = LoginActivity.database.rawQuery("select * from Ban where idTable=?",new String[]{idTable+""});
        while (c.moveToNext())
            str = c.getString(1);
        c.close();
        return str;
    }
    private void addListTable()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        while (c.moveToNext())
        {
            int count=0;
            Cursor cu = LoginActivity.database.rawQuery("select * from Bill",null);
            while (cu.moveToNext())
            {
                if (c.getInt(0)==cu.getInt(1) && cu.getInt(2)==1)
                    count++;
            }
            cu.close();
            ContentValues value = new ContentValues();
            value.put("idThongKeTable",autoThongKeTable());
            value.put("idTable",c.getInt(0));
            value.put("count",count);
            LoginActivity.database.insert("ThongKeTable",null,value);
        }
        c.close();
    }
    private int autoThongKeTable()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeTable",null);
        while(c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }
}
