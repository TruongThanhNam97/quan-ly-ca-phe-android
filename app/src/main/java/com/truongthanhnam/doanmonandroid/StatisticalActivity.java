package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.truongthanhnam.adapter.BillAdapter;
import com.truongthanhnam.adapter.TableAdapter;
import com.truongthanhnam.adapter.ViewFoodAdapter;
import com.truongthanhnam.model.Bill;
import com.truongthanhnam.model.Food;
import com.truongthanhnam.model.Table;
import com.truongthanhnam.model.ThongKeFood;
import com.truongthanhnam.model.ThongKeTable;

import java.util.ArrayList;

public class StatisticalActivity extends AppCompatActivity {

    TabHost tabHost;
    ListView lvFood;
    ViewFoodAdapter adapterFood;
    ListView lvTable;
    TableAdapter adapterTable;
    ListView lvBill;
    BillAdapter adapterBill;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistical);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId)
            {
                Animation a = AnimationUtils.loadAnimation(StatisticalActivity.this,R.anim.scale);
                if (tabId.equals("tab1"))
                {
                    Toast.makeText(StatisticalActivity.this, "Độ ưu chuộng của món ăn từ cao đến thấp", Toast.LENGTH_SHORT).show();
                    lvFood.setAnimation(a);
                }
                if (tabId.equals("tab2"))
                {
                    Toast.makeText(StatisticalActivity.this, "Độ ưa chuộng của bàn ăn từ cao đến thấp", Toast.LENGTH_SHORT).show();
                    lvTable.setAnimation(a);
                }
                if (tabId.equals("tab3"))
                {
                    Toast.makeText(StatisticalActivity.this, "Tổng tiền của từng bill từ cao đến thấp", Toast.LENGTH_SHORT).show();
                    lvBill.setAnimation(a);
                }
            }
        });
    }

    private void addControls()
    {
        tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("tab1");
        tab1.setContent(R.id.tab1);
        tab1.setIndicator("",getResources().getDrawable(R.drawable.food));
        tabHost.addTab(tab1);
        TabHost.TabSpec tab2 = tabHost.newTabSpec("tab2");
        tab2.setContent(R.id.tab2);
        tab2.setIndicator("",getResources().getDrawable(R.drawable.table2));
        tabHost.addTab(tab2);
        TabHost.TabSpec tab3 = tabHost.newTabSpec("tab3");
        tab3.setContent(R.id.tab3);
        tab3.setIndicator("",getResources().getDrawable(R.drawable.allbill));
        tabHost.addTab(tab3);

        lvFood = (ListView) findViewById(R.id.lvFood);
        adapterFood = new ViewFoodAdapter(StatisticalActivity.this,R.layout.itemviewfood);
        lvFood.setAdapter(adapterFood);

        lvTable = (ListView) findViewById(R.id.lvTable);
        adapterTable = new TableAdapter(StatisticalActivity.this,R.layout.itembanan);
        lvTable.setAdapter(adapterTable);

        lvBill = (ListView) findViewById(R.id.lvBill);
        adapterBill = new BillAdapter(StatisticalActivity.this,R.layout.itembill);
        lvBill.setAdapter(adapterBill);

    }
    private void addListFood()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Food",null);
        while (c.moveToNext())
        {
            int count =0;
            Cursor cu = LoginActivity.database.rawQuery("select * from BillInfo",null);
            while (cu.moveToNext())
            {
                if (cu.getInt(2)==c.getInt(0))
                    count++;
            }
            cu.close();
            ContentValues value = new ContentValues();
            value.put("idThongKeFood",autoIdThongKeFood());
            value.put("idFood",c.getInt(0));
            value.put("count",count);
            LoginActivity.database.insert("ThongKeFood",null,value);
        }
        c.close();
    }
    private void addListTable()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        while (c.moveToNext())
        {
            int count=0;
            Cursor cu = LoginActivity.database.rawQuery("select * from Bill",null);
            while (cu.moveToNext())
            {
                if (c.getInt(0)==cu.getInt(1) && cu.getInt(2)==1)
                    count++;
            }
            cu.close();
            ContentValues value = new ContentValues();
            value.put("idThongKeTable",autoThongKeTable());
            value.put("idTable",c.getInt(0));
            value.put("count",count);
            LoginActivity.database.insert("ThongKeTable",null,value);
        }
        c.close();
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        LoginActivity.database.delete("ThongKeFood",null,null);
        LoginActivity.database.delete("ThongKeTable",null,null);
        addListFood();
        addListTable();
        showFood();
        showTable();
        showBill();
        Toast.makeText(this, "Độ ưu chuộng của món ăn từ cao đến thấp", Toast.LENGTH_SHORT).show();
    }

    private void showBill()
    {
        adapterBill.clear();
        Cursor c = LoginActivity.database.query("Bill",null,"statusBill=?",new String[]{1+""},null,null,"totalPrice DESC");
        while (c.moveToNext())
        {
            adapterBill.add(new Bill(c.getInt(0), c.getInt(1), c.getInt(2), c.getDouble(3)));
        }
        c.close();
    }

    private void showTable()
    {
        adapterTable.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeTable order by count DESC",null);
        while (c.moveToNext())
        {
            adapterTable.add(new Table(c.getInt(1),getNameTableByIdTable(c.getInt(1)),getStatusTableByIdTable(c.getInt(1))));
        }
        c.close();
    }

    private void showFood()
    {
        adapterFood.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeFood order by count DESC",null);
        while (c.moveToNext())
        {
            adapterFood.add(new Food(c.getInt(1),getNameFoodByIdFood(c.getInt(1)),getIdFoodcategoryByIdFood(c.getInt(1)),getPriceFoodByIdFood(c.getInt(1))));
        }
        c.close();
    }
    private String getNameFoodByIdFood(int idFood)
    {
        String str="";
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood=?",new String[]{idFood+""});
        while (c.moveToNext())
            str = c.getString(1);
        c.close();
        return str;
    }
    private double getPriceFoodByIdFood(int idFood)
    {
        double price=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood=?",new String[]{idFood+""});
        while (c.moveToNext())
            price = c.getDouble(3);
        return price;
    }
    private int getIdFoodcategoryByIdFood(int idFood)
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood=?",new String[]{idFood+""});
        while (c.moveToNext())
            id = c.getInt(2);
        return id;
    }
    private String getNameTableByIdTable(int idTable)
    {
        String str="";
        Cursor c = LoginActivity.database.rawQuery("select * from Ban where idTable=?",new String[]{idTable+""});
        while (c.moveToNext())
            str = c.getString(1);
        c.close();
        return str;
    }
    private String getStatusTableByIdTable(int idTable)
    {
        String str = "";
        Cursor c = LoginActivity.database.rawQuery("select * from Ban where idTable=?",new String[]{idTable+""});
        while (c.moveToNext())
            str = c.getString(2);
        return str;
    }
    private int autoIdThongKeFood()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeFood",null);
        while (c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }
    private int autoThongKeTable()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from ThongKeTable",null);
        while(c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }

}
