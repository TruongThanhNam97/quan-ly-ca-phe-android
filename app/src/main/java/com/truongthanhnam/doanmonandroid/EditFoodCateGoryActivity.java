package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

public class EditFoodCateGoryActivity extends AppCompatActivity {

    EditText edtIdFoodCateGoryEdit,edtNameFoodCateGoryEdit;
    Button btnEditFoodCateGory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_food_cate_gory);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        btnEditFoodCateGory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIdFoodCateGoryEdit.getText().toString().isEmpty() || edtNameFoodCateGoryEdit.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(EditFoodCateGoryActivity.this);
                    b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                    b.setTitle("Thông báo");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq1 = isCheckExistNameFoodCateGory(edtNameFoodCateGoryEdit.getText().toString());
                    if (kq1)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(EditFoodCateGoryActivity.this);
                        b.setMessage("Tên danh mục này đã tồn tại rồi, vui lòng nhập lại !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        boolean kq2 = updateFoodCateGory(Integer.parseInt(edtIdFoodCateGoryEdit.getText().toString()),edtNameFoodCateGoryEdit.getText().toString());
                        if (kq2)
                        {
                            Toast.makeText(EditFoodCateGoryActivity.this, "Update danh mục thành công !", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else
                        {
                            Toast.makeText(EditFoodCateGoryActivity.this, "Update danh mục thất bại !", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
            }
        });
    }

    private void addControls() {
        edtIdFoodCateGoryEdit = (EditText) findViewById(R.id.edtIdEditFoodCateGory);
        edtNameFoodCateGoryEdit = (EditText) findViewById(R.id.edtNameFoodCateGoryEdit);
        edtIdFoodCateGoryEdit.setText(ViewFoodCateGoryMainActivity.selectedFoodCateGory.getIdFoodCateGory()+"");
        edtIdFoodCateGoryEdit.setEnabled(false);
        edtNameFoodCateGoryEdit.setText(ViewFoodCateGoryMainActivity.selectedFoodCateGory.getNameFoodCateGory());
        edtNameFoodCateGoryEdit.requestFocus();
        btnEditFoodCateGory = (Button) findViewById(R.id.btnEditFoodCateGory);
    }
    private boolean isCheckExistNameFoodCateGory(String name)
    {
        if (name.equals(ViewFoodCateGoryMainActivity.selectedFoodCateGory.getNameFoodCateGory()))
            return false;
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        while (c.moveToNext())
        {
            if (c.getString(1).toLowerCase().equals(name.toLowerCase()))
                return true;
        }
        return false;
    }
    private int autoIdFoodCateGory()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        while (c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }
    private  boolean updateFoodCateGory(int idFoodCateGory,String nameFoodCateGory)
    {
        ContentValues value = new ContentValues();
        value.put("nameFoodCateGory",nameFoodCateGory);
        int kq = LoginActivity.database.update("FoodCateGory",value,"idFoodCateGory=?",new String[] {idFoodCateGory+""});
        if (kq>0)
            return true;
        return false;
    }

}
