package com.truongthanhnam.doanmonandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.truongthanhnam.adapter.HouseAdapter;
import com.truongthanhnam.model.House;

public class HouseActivity extends AppCompatActivity {

    ListView lvHouse;
    HouseAdapter adapterHouse;
    public static House seletedHouse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house);
        addControls();
        addEvents();
    }

    private void addEvents() {
        lvHouse.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                seletedHouse = adapterHouse.getItem(position);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }

    private void addControls() {
        lvHouse = (ListView) findViewById(R.id.lvHouse);
        adapterHouse = new HouseAdapter(HouseActivity.this,R.layout.itemhouse);
        lvHouse.setAdapter(adapterHouse);
        registerForContextMenu(lvHouse);
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayData();
    }

    private void displayData()
    {
        adapterHouse.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from House",null);
        while (c.moveToNext())
        {
            adapterHouse.add(new House(c.getInt(0),c.getString(1),c.getFloat(2),c.getFloat(3),c.getBlob(4)));
        }
        c.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionhouse,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearchHouse);
        SearchView searchView = (SearchView) mnu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    displayData();
                }
                else
                {
                    adapterHouse.clear();
                    Cursor c = LoginActivity.database.rawQuery("select * from House",null);
                    while (c.moveToNext())
                    {
                        if (c.getString(1).toLowerCase().trim().contains(newText.toLowerCase().trim()))
                            adapterHouse.add(new House(c.getInt(0),c.getString(1),c.getFloat(2),c.getFloat(3),c.getBlob(4)));
                    }
                    c.close();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuAddHouse)
        {
            Intent intent = new Intent(HouseActivity.this,AddHouseActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuRoadSupport)
        {
            Intent intent = new Intent(HouseActivity.this,RoadSupportActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contexthouse,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuEditHouse)
        {
            Intent intent = new Intent(HouseActivity.this,EditHouseActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuDeleteHouse)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(HouseActivity.this);
            b.setTitle("Hỏi xóa");
            b.setMessage("Bạn có chắc muốn xóa "+seletedHouse.getNameHouse()+" hay không ?");
            b.setIcon(android.R.drawable.ic_dialog_info);
            b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LoginActivity.database.delete("House","idHouse=?",new String[]{seletedHouse.getIdHouse()+""});
                    Toast.makeText(HouseActivity.this, "Xóa thành công", Toast.LENGTH_SHORT).show();
                    onResume();
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = b.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        return super.onContextItemSelected(item);
    }
}
