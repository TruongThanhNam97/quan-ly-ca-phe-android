package com.truongthanhnam.doanmonandroid;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class WelcomeAppActivity extends AppCompatActivity {

    ImageView txtAbout,txtHelp,txtNhanVien,txtPhaChe,imgChart,imgHouse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_app);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        txtAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeAppActivity.this,ThongTinGroupActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exist);
            }
        });
        txtHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeAppActivity.this,HelpActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exist);
            }
        });
        txtNhanVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeAppActivity.this,MenuActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exist);
            }
        });
        txtPhaChe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeAppActivity.this,PhaCheActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exist);
            }
        });
        imgChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeAppActivity.this,ChartActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exist);
            }
        });
        imgHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeAppActivity.this,HouseActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter,R.anim.exist);
            }
        });
    }

    private void addControls()
    {
        txtAbout = (ImageView) findViewById(R.id.txtAbout);
        txtHelp = (ImageView) findViewById(R.id.txtHelp);
        txtNhanVien = (ImageView) findViewById(R.id.txtNhanVien);
        txtPhaChe = (ImageView) findViewById(R.id.txtPhaChe);
        imgChart = (ImageView) findViewById(R.id.imgChart);
        imgHouse = (ImageView) findViewById(R.id.imgHouse);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //Animation a = AnimationUtils.loadAnimation(WelcomeAppActivity.this,R.anim.ani);
        //imgNext.setAnimation(a);
        //imgNext.setBackgroundResource(R.drawable.test);
        //AnimationDrawable a = (AnimationDrawable) imgNext.getBackground();
        //a.start();
        if (LoginActivity.phanQuyen==0) //nhân viên
        {
            txtHelp.setVisibility(View.INVISIBLE);
            txtPhaChe.setVisibility(View.INVISIBLE);
            txtAbout.setVisibility(View.INVISIBLE);
            imgChart.setVisibility(View.INVISIBLE);
            imgHouse.setVisibility(View.INVISIBLE);
        }
        else if (LoginActivity.phanQuyen==1) // pha chế
        {
            txtAbout.setVisibility(View.INVISIBLE);
            txtNhanVien.setVisibility(View.INVISIBLE);
            txtHelp.setVisibility(View.INVISIBLE);
            imgChart.setVisibility(View.INVISIBLE);
            imgHouse.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }
}
