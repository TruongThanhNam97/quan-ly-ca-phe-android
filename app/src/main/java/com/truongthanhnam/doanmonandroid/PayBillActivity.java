package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DAL.OrderInfoDAL;
import com.truongthanhnam.DTO.OrderInfo;
import com.truongthanhnam.adapter.BillInfoAdapter;
import com.truongthanhnam.model.BillInfo;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class PayBillActivity extends AppCompatActivity {
    ListView lvBillInfo;
    BillInfoAdapter adapter;
    TextView txtTotalPrice,txtSale,txtChange;
    EditText edtCash;
    Button btnPay;
    TimerTask timerTask;
    Timer timer;
    ImageView imgDrainSale,imgMinusSale;
    public static BillInfo selectedBillInfo;
    double priceSale=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_bill);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MenuActivity.selectedTable.getStatusTable().equals("Trống"))
                {
                    Toast.makeText(PayBillActivity.this, "Bàn có ai đâu mà thanh toán", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    if (priceSale!=0.0)
                    {
                        ContentValues value = new ContentValues();
                        value.put("totalPrice",priceSale);
                        LoginActivity.database.update("Bill",value,"idBill=?",new String[]{getIdBillByIdTableAndStatusBill()+""});
                    }
                        OrderInfoDAL.deleteOrderInfoByIdOrder(getIdBillByIdTableAndStatusBill());
                        OrderDAL.deleteOrder(getIdBillByIdTableAndStatusBill());
                        updateStatusBill();
                        updateStatusTable();
                        Toast.makeText(PayBillActivity.this, "Hóa đơn " + MenuActivity.selectedTable.getNameTable() + " đã được thanh toán", Toast.LENGTH_SHORT).show();
                        try {
                            AssetFileDescriptor afd = getAssets().openFd("musics/" + "pay.wav");
                            MediaPlayer play = new MediaPlayer();
                            play.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                            play.prepare();
                            play.start();
                        } catch (Exception ex) {
                            Log.e("Error:", ex.toString());
                        }
                }
            }
        });
        lvBillInfo.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedBillInfo = adapter.getItem(position);
                return false;
            }
        });
        imgDrainSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count=0;
                count = Integer.parseInt(txtSale.getText().toString().replace('%',' ').trim());
                count+=5;
                if (count==100)
                {
                    txtSale.setText(count+"%");
                    priceSale = getTotalPriceByIdBill();
                    priceSale = priceSale*((100.0-(count*1.0))/100.0);
                    DecimalFormat df = new DecimalFormat("#,###");
                    DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
                    dfs.setGroupingSeparator('.');
                    df.setDecimalFormatSymbols(dfs);
                    txtTotalPrice.setText(df.format(priceSale)+" VNĐ");
                }
                else if (count>100)
                {
                    count=100;
                    txtSale.setText(count+"%");
                    priceSale = getTotalPriceByIdBill();
                    priceSale = priceSale*((100.0-(count*1.0))/100.0);
                    DecimalFormat df = new DecimalFormat("#,###");
                    DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
                    dfs.setGroupingSeparator('.');
                    df.setDecimalFormatSymbols(dfs);
                    txtTotalPrice.setText(df.format(priceSale)+" VNĐ");
                }
                else
                {
                    txtSale.setText(count+"%");
                    priceSale = getTotalPriceByIdBill();
                    priceSale = priceSale*((100.0-(count*1.0))/100.0);
                    DecimalFormat df = new DecimalFormat("#,###");
                    DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
                    dfs.setGroupingSeparator('.');
                    df.setDecimalFormatSymbols(dfs);
                    txtTotalPrice.setText(df.format(priceSale)+" VNĐ");
                }
            }
        });
        imgMinusSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count=0;
                count = Integer.parseInt(txtSale.getText().toString().replace('%',' ').trim());
                count-=5;
                if (count<0)
                    count=0;
                txtSale.setText(count+"%");
                priceSale = getTotalPriceByIdBill();
                priceSale = priceSale*((100.0-(count*1.0))/100.0);
                DecimalFormat df = new DecimalFormat("#,###");
                DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
                dfs.setGroupingSeparator('.');
                df.setDecimalFormatSymbols(dfs);
                txtTotalPrice.setText(df.format(priceSale)+" VNĐ");
            }
        });
    }

    private void updateStatusBill()
    {
        ContentValues value = new ContentValues();
        value.put("statusBill",1);
        LoginActivity.database.update("Bill",value,"idBill=?",new String[]{getIdBillByIdTableAndStatusBill()+""});
    }

    private void updateStatusTable()
    {
        ContentValues value = new ContentValues();
        value.put("statusTable","Trống");
        LoginActivity.database.update("Ban",value,"idTable=?",new String[]{MenuActivity.selectedTable.getIdTable()+""});
        finish();
    }

    private void getBillInfoByIdBillToListView()
    {
        adapter.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+getIdBillByIdTableAndStatusBill()+"",null);
        while (c.moveToNext())
            adapter.add(new BillInfo(c.getInt(0),c.getInt(1),c.getInt(2),c.getInt(3)));
        c.close();
    }

    private void addControls() {
        lvBillInfo = (ListView) findViewById(R.id.lvBillInfo);
        adapter = new BillInfoAdapter(PayBillActivity.this,R.layout.itembillinfo);
        lvBillInfo.setAdapter(adapter);
        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);
        btnPay = (Button) findViewById(R.id.btnPay);
        txtSale = (TextView) findViewById(R.id.txtSale);
        imgDrainSale = (ImageView) findViewById(R.id.imgDrainSale);
        imgMinusSale = (ImageView) findViewById(R.id.imgMinusSale);
        edtCash = (EditText) findViewById(R.id.edtCash);
        txtChange = (TextView) findViewById(R.id.txtChange);
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtTotalPrice.setText(df.format(getTotalPriceByIdBill())+" VNĐ");
        txtChange.setText(0+" VNĐ");
        priceSale = getTotalPriceByIdBill();
        registerForContextMenu(lvBillInfo);
    }
    private int getIdBillByIdTableAndStatusBill()
    {
        int id=0;
        Cursor c = LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{MenuActivity.selectedTable.getIdTable()+"",0+""},null,null,null);
        while (c.moveToNext())
            id = c.getInt(0);
        c.close();
        return id;
    }
    private double getTotalPriceByIdBill()
    {
        double price=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Bill where idBill="+getIdBillByIdTableAndStatusBill()+"",null);
        while (c.moveToNext())
            price = c.getDouble(3);
        c.close();
        return price;
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtSale.setText(0+"%");
        getBillInfoByIdBillToListView();
        updateToTalPriceOfBill();
        final DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtTotalPrice.setText(df.format(getTotalPriceByIdBill())+" VNĐ");
        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (edtCash.getText().toString().isEmpty()==false)
                        {
                            double cash = Double.parseDouble(edtCash.getText().toString().trim());
                            double totalprice = priceSale;
                            double change = cash-totalprice;
                            txtChange.setText(df.format(change)+" VNĐ");
                        }
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask,0,500);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextbillinfo,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuDeleteBillInfo)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(PayBillActivity.this);
            b.setTitle("Hỏi xóa");
            b.setMessage("Bạn có chắc chắn muốn xóa "+getNameFoodByIdFood(selectedBillInfo.getIdFood())+" ra khỏi hóa đơn ?");
            b.setIcon(android.R.drawable.ic_dialog_info);
            b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    OrderInfoDAL.deleteOrderInfoByIdOrderAndIdFood(selectedBillInfo.getIdFood(),selectedBillInfo.getIdBill());
                    LoginActivity.database.delete("BillInfo","idBillInfo=?",new String[]{selectedBillInfo.getIdBillInfo()+""});
                    getBillInfoByIdBillToListView();
                    updateToTalPriceOfBill();
                    DecimalFormat df = new DecimalFormat("#,###");
                    DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
                    dfs.setGroupingSeparator('.');
                    df.setDecimalFormatSymbols(dfs);
                    txtTotalPrice.setText(df.format(getTotalPriceByIdBill())+" VNĐ");
                    Toast.makeText(PayBillActivity.this, "Delete thành công", Toast.LENGTH_SHORT).show();
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = b.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        if (item.getItemId()==R.id.menuChangeCount)
        {
            Intent intent = new Intent(PayBillActivity.this,ChangeCountActivity.class);
            startActivity(intent);
        }
        return super.onContextItemSelected(item);
    }
    private String getNameFoodByIdFood(int id)
    {
        String str="";
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            str = c.getString(1);
        c.close();
        return str;
    }
    private double getTotalPrice()
    {
        double totalPrice=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+getIdBillByIdTableAndStatusBill()+"",null);
        while (c.moveToNext())
            totalPrice+=c.getInt(3)*1.0*getPriceFoodByIdFood(c.getInt(2));
        return totalPrice;
    }
    private double getPriceFoodByIdFood(int id)
    {
        double price = 0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            price = c.getDouble(3);
        return price;
    }
    private void updateToTalPriceOfBill()
    {
        ContentValues value = new ContentValues();
        value.put("totalPrice",getTotalPrice());
        LoginActivity.database.update("Bill",value,"idBill=?",new String[]{getIdBillByIdTableAndStatusBill()+""});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionbillinfo,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearchPay);
        SearchView searchView = (SearchView) mnu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    getBillInfoByIdBillToListView();
                }
                else
                {
                    adapter.clear();
                    Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+getIdBillByIdTableAndStatusBill()+"",null);
                    while (c.moveToNext())
                    {
                        if (getNameFoodByIdFood(c.getInt(2)).toLowerCase().contains(newText.toLowerCase()) || (getPriceFoodByIdFood(c.getInt(2))+"").contains(newText))
                            adapter.add(new BillInfo(c.getInt(0),c.getInt(1),c.getInt(2),c.getInt(3)));
                    }
                    c.close();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
