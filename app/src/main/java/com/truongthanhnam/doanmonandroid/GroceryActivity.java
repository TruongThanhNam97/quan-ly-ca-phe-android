package com.truongthanhnam.doanmonandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DTO.Order;
import com.truongthanhnam.adapter.NhanMonAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GroceryActivity extends AppCompatActivity {

    ListView lvGrocery;
    NhanMonAdapter adapter;
    TimerTask timerTask;
    Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery);
        addControls();
    }

    private void addControls() {
        lvGrocery = (ListView) findViewById(R.id.lvGrocery);
        adapter = new NhanMonAdapter(GroceryActivity.this,R.layout.itemnhanmon);
        lvGrocery.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerTask = new TimerTask() {
            @Override
            public void run() {
               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       displayData();
                   }
               });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask,0,2000);
    }

    private void displayData()
    {
        adapter.clear();
        ArrayList<Order> ds = OrderDAL.getListOrderToServe();
        for(Order x : ds)
            adapter.add(x);
    }
}
