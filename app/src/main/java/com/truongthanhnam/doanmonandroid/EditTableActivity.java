package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.truongthanhnam.DAL.TableeDAL;

public class EditTableActivity extends AppCompatActivity {

    EditText edtEditTable;
    Button btnUpdateTable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_table);
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnUpdateTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEditTable.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(EditTableActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Bạn chưa nhập đủ thông tin");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    if (edtEditTable.getText().toString().toLowerCase().equals(MenuActivity.selectedTable.getNameTable().toLowerCase()))
                    {
                        TableeDAL.editTable(MenuActivity.selectedTable.getIdTable(),edtEditTable.getText().toString());
                        updateNameTable();
                        Toast.makeText(EditTableActivity.this, "Update tên bàn thành công", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        boolean kq = isExistNameTable();
                        if (kq)
                        {
                            AlertDialog.Builder b = new AlertDialog.Builder(EditTableActivity.this);
                            b.setTitle("Thông báo");
                            b.setMessage("Tên bàn đã tồn tại, vui lòng nhập lại !");
                            b.setIcon(android.R.drawable.ic_dialog_info);
                            b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = b.create();
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();
                        }
                        else
                        {
                            TableeDAL.editTable(MenuActivity.selectedTable.getIdTable(),edtEditTable.getText().toString());
                            updateNameTable();
                            Toast.makeText(EditTableActivity.this, "Update tên bàn thành công", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
            }
        });
    }

    private void addControls() {
        edtEditTable = (EditText) findViewById(R.id.edtEditTable);
        btnUpdateTable = (Button) findViewById(R.id.btnUpdateTable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtEditTable.setText(MenuActivity.selectedTable.getNameTable());
    }
    private boolean isExistNameTable()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        while (c.moveToNext())
        {
            if (c.getString(1).toLowerCase().equals(edtEditTable.getText().toString().toLowerCase()))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    private void updateNameTable()
    {
        ContentValues value = new ContentValues();
        value.put("nameTable",edtEditTable.getText().toString());
        LoginActivity.database.update("Ban",value,"idTable=?",new String[]{MenuActivity.selectedTable.getIdTable()+""});
    }
}
