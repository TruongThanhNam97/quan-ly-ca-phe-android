package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.truongthanhnam.DAL.FooddDAL;
import com.truongthanhnam.model.FoodCateGory;

public class EditFoodActivity extends AppCompatActivity {

    EditText edtIdFoodEdit,edtNameFoodEdit,edtPriceFoodEdit;
    Spinner spinnerFoodCateGoryEdit;
    ArrayAdapter<FoodCateGory> adapterFoodCateGory;
    Button btnEditFood;
    FoodCateGory selectedFoodCateGory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_food);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        spinnerFoodCateGoryEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFoodCateGory = adapterFoodCateGory.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnEditFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIdFoodEdit.getText().toString().isEmpty()||edtNameFoodEdit.getText().toString().isEmpty()||edtPriceFoodEdit.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(EditFoodActivity.this);
                    b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                    b.setTitle("Thông báo");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq1 = isCheckExistNameFood(edtNameFoodEdit.getText().toString());
                    if (kq1)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(EditFoodActivity.this);
                        b.setMessage("Tên món ăn này tồn tại rồi, vui lòng nhập lại !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        FooddDAL.updateNameFooddByIdFoodd(Integer.parseInt(edtIdFoodEdit.getText().toString()),edtNameFoodEdit.getText().toString());
                        boolean kq2 = updateFood(Integer.parseInt(edtIdFoodEdit.getText().toString()),edtNameFoodEdit.getText().toString(),selectedFoodCateGory.getIdFoodCateGory(),Double.parseDouble(edtPriceFoodEdit.getText().toString()));
                        if (kq2)
                        {
                            Toast.makeText(EditFoodActivity.this, "Update món ăn thành công !", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else
                        {
                            Toast.makeText(EditFoodActivity.this, "Update món ăn thất bại !", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }
            }
        });
    }

    private void addControls()
    {
        edtIdFoodEdit = (EditText) findViewById(R.id.edtIdFoodEdit);
        edtNameFoodEdit = (EditText) findViewById(R.id.edtNameFoodEdit);
        edtPriceFoodEdit = (EditText) findViewById(R.id.edtPriceFoodEdit);
        spinnerFoodCateGoryEdit = (Spinner) findViewById(R.id.spinnerFoodCateGoryEdit);
        adapterFoodCateGory = new ArrayAdapter<FoodCateGory>(EditFoodActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterFoodCateGory.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerFoodCateGoryEdit.setAdapter(adapterFoodCateGory);
        btnEditFood = (Button) findViewById(R.id.btnEditFood);
        addFoodCateGoryToSpinner();
        edtIdFoodEdit.setText(ViewFoodActivity.selectedFood.getIdFood()+"");
        edtIdFoodEdit.setEnabled(false);
        edtNameFoodEdit.setText(ViewFoodActivity.selectedFood.getNameFood());
        edtPriceFoodEdit.setText(ViewFoodActivity.selectedFood.getPrice()+"");
    }

    private void addFoodCateGoryToSpinner() {
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGoRy",null);
        adapterFoodCateGory.clear();
        while (c.moveToNext())
            adapterFoodCateGory.add(new FoodCateGory(c.getInt(0),c.getString(1)));
        c.close();
    }
    private boolean isCheckExistNameFood(String name)
    {
        if (name.equals(ViewFoodActivity.selectedFood.getNameFood()))
            return false;
        Cursor c = LoginActivity.database.rawQuery("select * from Food",null);
        while (c.moveToNext())
        {
            if (c.getString(1).toLowerCase().equals(name.toLowerCase()))
                return true;
        }
        return false;
    }
    private boolean updateFood(int idFood,String nameFood,int idFoodCateGory,double price)
    {
        ContentValues value = new ContentValues();
        value.put("nameFood",nameFood);
        value.put("idFoodCateGory",idFoodCateGory);
        value.put("price",price);
        int kq = LoginActivity.database.update("Food",value,"idFood=?",new String[] {idFood+""});
        if (kq>0)
            return true;
        return false;
    }
}
