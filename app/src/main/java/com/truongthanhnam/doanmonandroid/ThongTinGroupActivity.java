package com.truongthanhnam.doanmonandroid;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

import com.truongthanhnam.adapter.SinhVienAdapter;
import com.truongthanhnam.model.SinhVien;

public class ThongTinGroupActivity extends AppCompatActivity {

    ListView lvThongTinNhom;
    SinhVienAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_group);
        addControls();
    }

    private void addControls()
    {
        lvThongTinNhom = (ListView) findViewById(R.id.lvThongTinNhom);
        adapter = new SinhVienAdapter(ThongTinGroupActivity.this,R.layout.itemsinhvien);
        lvThongTinNhom.setAdapter(adapter);
        adapter.add(new SinhVien("Trương Thanh Nam","15520526"));
        adapter.add(new SinhVien("Đặng Xuân Duy Khương","15520389"));
        adapter.add(new SinhVien("Nguyễn Hoàng Phúc","15520645"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Animation a = AnimationUtils.loadAnimation(ThongTinGroupActivity.this,R.anim.alpha);
        lvThongTinNhom.setAnimation(a);
        try
        {
            AssetFileDescriptor afd = getAssets().openFd("musics/"+"tiger.wav");
            MediaPlayer play = new MediaPlayer();
            play.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            play.prepare();
            play.start();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }
}
