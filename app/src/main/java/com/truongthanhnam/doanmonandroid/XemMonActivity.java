package com.truongthanhnam.doanmonandroid;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.truongthanhnam.DAL.OrderInfoDAL;
import com.truongthanhnam.DTO.Order;
import com.truongthanhnam.DTO.OrderInfo;
import com.truongthanhnam.adapter.XemMonAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class XemMonActivity extends AppCompatActivity {
    ListView lvXemMon;
    XemMonAdapter adapter;
    Timer timer;
    TimerTask timerTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xem_mon);
        addControls();
    }

    private void addControls() {
        lvXemMon = (ListView) findViewById(R.id.lvXemMonAn);
        adapter = new XemMonAdapter(XemMonActivity.this,R.layout.itemorderinfo);
        lvXemMon.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayData();
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask,0,2000);
    }

    private void displayData() {
        adapter.clear();
        ArrayList<OrderInfo> ds = new ArrayList<>();
        ds = OrderInfoDAL.getListOrderInfoByIdOrder(PhaCheActivity.seletedOrder.getIdOrder());
        for(OrderInfo x : ds)
            adapter.add(x);
    }
    public String getNameFoodByIdFood(int idFood)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood=?",new String[]{idFood+""});
        String str="";
        while (c.moveToNext())
        {
            str = c.getString(1);
        }
        c.close();
        return str;
    }
}
