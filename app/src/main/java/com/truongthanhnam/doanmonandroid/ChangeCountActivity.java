package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DAL.OrderInfoDAL;
import com.truongthanhnam.DTO.OrderInfo;

public class ChangeCountActivity extends AppCompatActivity {

    TextView txtChangeCount;
    ImageView imgUpCount,imgDownCount;
    Button btnUpdateCount;
    int x=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_count);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        imgUpCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(txtChangeCount.getText().toString());
                count++;
                txtChangeCount.setText(count+"");
            }
        });
        imgDownCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(txtChangeCount.getText().toString());
                count--;
                if (count<1)
                    count=1;
                txtChangeCount.setText(count+"");
            }
        });
        btnUpdateCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDAL.updateStatusOrderByIdOrder(getIdBillByIdTableAndStatusBill(),0);
                int y = Integer.parseInt(txtChangeCount.getText().toString());
                 if (x<y)
                 {
                     x=y-x;
                     boolean kq = OrderInfoDAL.isExistOrderInfoByIdOrderAndIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill());
                     if (kq)
                         OrderInfoDAL.updateCountOrderInfoInOrderInfo(OrderInfoDAL.getIdBillInfoByIdOrderAndIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill()),OrderInfoDAL.getCountOrderInfoByIdOrderAmdIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill())+x);
                     else
                         OrderInfoDAL.addOrderInfo(OrderInfoDAL.autoIdOrderInfo(),PayBillActivity.selectedBillInfo.getIdBill(),PayBillActivity.selectedBillInfo.getIdFood(),x);
                 }
                else if (x>y)
                 {
                     x=x-y;
                     if (OrderInfoDAL.getCountOrderInfoByIdOrderAmdIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill())-x>=1)
                         OrderInfoDAL.updateCountOrderInfoInOrderInfo(OrderInfoDAL.getIdBillInfoByIdOrderAndIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill()),OrderInfoDAL.getCountOrderInfoByIdOrderAmdIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill())-x);
                     else
                         OrderInfoDAL.deleteOrderInfoByIdOrderAndIdFood(PayBillActivity.selectedBillInfo.getIdFood(),PayBillActivity.selectedBillInfo.getIdBill());
                 }
                ContentValues value = new ContentValues();
                value.put("count",Integer.parseInt(txtChangeCount.getText().toString()));
                LoginActivity.database.update("BillInfo",value,"idBillInfo=?",new String[]{PayBillActivity.selectedBillInfo.getIdBillInfo()+""});
                Toast.makeText(ChangeCountActivity.this, "Update thành công", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void addControls() {
        txtChangeCount = (TextView) findViewById(R.id.txtChangeCount);
        imgUpCount = (ImageView) findViewById(R.id.imgUpCount);
        imgDownCount = (ImageView) findViewById(R.id.imgDownCount);
        btnUpdateCount = (Button) findViewById(R.id.btnUpdateChangeCount);
        txtChangeCount.setText(PayBillActivity.selectedBillInfo.getCount()+"");
        x=Integer.parseInt(PayBillActivity.selectedBillInfo.getCount()+"");
    }
    private int getIdBillByIdTableAndStatusBill()
    {
        int id=0;
        Cursor c = LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{MenuActivity.selectedTable.getIdTable()+"",0+""},null,null,null);
        while (c.moveToNext())
            id = c.getInt(0);
        c.close();
        return id;
    }
}
