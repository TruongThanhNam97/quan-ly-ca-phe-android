package com.truongthanhnam.doanmonandroid;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.truongthanhnam.DAL.TableeDAL;
import com.truongthanhnam.adapter.TableAdapter;
import com.truongthanhnam.model.Table;

public class MenuActivity extends AppCompatActivity {

    ListView lvBanAn;
    TableAdapter adapter;
    public static Table selectedTable;
    Button btnGrocery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        lvBanAn.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedTable = adapter.getItem(position);
                return false;
            }
        });
        btnGrocery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,GroceryActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addControls()
    {
        lvBanAn = (ListView) findViewById(R.id.lvBanAn);
        adapter = new TableAdapter(MenuActivity.this,R.layout.itembanan);
        lvBanAn.setAdapter(adapter);
        btnGrocery = (Button) findViewById(R.id.btnGrocery);
        registerForContextMenu(lvBanAn);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearch);
        SearchView seachView = (SearchView) mnu.getActionView();
        seachView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    adapter.clear();
                    Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
                    while (c.moveToNext())
                        adapter.add(new Table(c.getInt(0),c.getString(1),c.getString(2)));
                    c.close();
                }
                else
                {
                    adapter.clear();
                    Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
                    while (c.moveToNext())
                    {
                        if (c.getString(1).toLowerCase().contains(newText.toLowerCase()) || c.getString(2).toLowerCase().contains(newText.toLowerCase()))
                        {
                            adapter.add(new Table(c.getInt(0),c.getString(1),c.getString(2)));
                        }
                    }
                    c.close();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuAddTable)
        {
            Intent intent = new Intent(MenuActivity.this,AddTableActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuAddFoodCateGory)
        {
            Intent intent = new Intent(MenuActivity.this,AddFoodCateGoryActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuAddFood)
        {
            Intent intent = new Intent(MenuActivity.this,AddFoodActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        adapter.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        while (c.moveToNext())
        {
            adapter.add(new Table(c.getInt(0),c.getString(1),c.getString(2)));
        }
        c.close();
       // Animation a = AnimationUtils.loadAnimation(MenuActivity.this,R.anim.shrink);
        //lvBanAn.setAnimation(a);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenutable,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuOrder)
        {
            Intent intent = new Intent(MenuActivity.this,OrderActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuPayBill)
        {
            Intent intent = new Intent(MenuActivity.this,PayBillActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuSwap)
        {
            Intent intent = new Intent(MenuActivity.this,SwapTableActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuDeleteTable)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(MenuActivity.this);
            b.setTitle("Hỏi xóa");
            b.setMessage("Bạn có chắc muốn xóa "+selectedTable.getNameTable()+" ?");
            b.setIcon(android.R.drawable.ic_dialog_info);
            b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    TableeDAL.deleteTable(selectedTable.getIdTable());
                    LoginActivity.database.delete("Ban","idTable=?",new String[]{selectedTable.getIdTable()+""});
                    onResume();
                    Toast.makeText(MenuActivity.this, "Xóa "+selectedTable.getNameTable()+" thành công", Toast.LENGTH_SHORT).show();
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = b.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        if (item.getItemId()==R.id.menuEditTable)
        {
            Intent intent = new Intent(MenuActivity.this,EditTableActivity.class);
            startActivity(intent);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }
}
