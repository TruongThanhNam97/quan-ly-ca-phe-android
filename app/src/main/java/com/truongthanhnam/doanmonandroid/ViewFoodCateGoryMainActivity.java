package com.truongthanhnam.doanmonandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.truongthanhnam.adapter.ViewFoodCateGoryAdapter;
import com.truongthanhnam.model.FoodCateGory;

public class ViewFoodCateGoryMainActivity extends AppCompatActivity {

    ListView lvViewFoodCateGory;
    ViewFoodCateGoryAdapter adapter;
    public static FoodCateGory selectedFoodCateGory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_food_cate_gory_main);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        lvViewFoodCateGory.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedFoodCateGory = adapter.getItem(position);
                return false;
            }
        });
    }

    private void addControls() {
        lvViewFoodCateGory = (ListView) findViewById(R.id.lvViewFoodCateGory);
        adapter = new ViewFoodCateGoryAdapter(ViewFoodCateGoryMainActivity.this,R.layout.itemviewfoodcategory);
        lvViewFoodCateGory.setAdapter(adapter);
        registerForContextMenu(lvViewFoodCateGory);
    }

    private void addFoodCateGoryToListView()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        adapter.clear();
        while (c.moveToNext())
            adapter.add(new FoodCateGory(c.getInt(0),c.getString(1)));
        c.close();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenuviewfoodcategory,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuEditFoodCateGory)
        {
            Intent intent = new Intent(ViewFoodCateGoryMainActivity.this,EditFoodCateGoryActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuDeleteViewFoodCateGory)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(ViewFoodCateGoryMainActivity.this);
            b.setTitle("Hỏi xóa");
            b.setMessage("Bạn có chắc muốn xóa danh mục : "+selectedFoodCateGory.getNameFoodCateGory()+" ?");
            b.setIcon(android.R.drawable.ic_dialog_info);
            b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int kq = LoginActivity.database.delete("FoodCateGory","idFoodCateGory=?",new String[]{selectedFoodCateGory.getIdFoodCateGory()+""});
                    if (kq>0)
                    {
                        Toast.makeText(ViewFoodCateGoryMainActivity.this, "Xóa danh mục "+selectedFoodCateGory.getNameFoodCateGory()+" thành công !", Toast.LENGTH_SHORT).show();
                        onResume();
                    }
                    else
                    {
                        Toast.makeText(ViewFoodCateGoryMainActivity.this, "Xóa danh mục "+selectedFoodCateGory.getNameFoodCateGory()+" thất bại !", Toast.LENGTH_SHORT).show();
                        onResume();
                    }
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = b.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        addFoodCateGoryToListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenuviewfood,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearchViewFood);
        SearchView searchView = (SearchView) mnu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    addFoodCateGoryToListView();
                }
                else
                {
                    adapter.clear();
                    Cursor c = LoginActivity.database.query("FoodCateGory",null,"nameFoodCateGory like ?",new String[]{"%"+newText+"%"},null,null,null);
                    while (c.moveToNext())
                        adapter.add(new FoodCateGory(c.getInt(0),c.getString(1)));
                    c.close();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
