package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddFoodCateGoryActivity extends AppCompatActivity {

    EditText edtIdFoodCateGory,edtNameFoodCateGory;
    Button btnAddFoodCateGory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food_cate_gory);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        btnAddFoodCateGory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIdFoodCateGory.getText().toString().isEmpty() || edtNameFoodCateGory.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(AddFoodCateGoryActivity.this);
                    b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                    b.setTitle("Thông báo");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq1 = isCheckExistNameFoodCateGory(edtNameFoodCateGory.getText().toString());
                    if (kq1)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(AddFoodCateGoryActivity.this);
                        b.setMessage("Tên này đã tồn tại, vui lòng nhập lại !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        boolean kq2 = addFoodCateGory(Integer.parseInt(edtIdFoodCateGory.getText().toString()),edtNameFoodCateGory.getText().toString());
                        if (kq2)
                        {
                            Toast.makeText(AddFoodCateGoryActivity.this, "Thêm danh mục món ăn thành công !", Toast.LENGTH_SHORT).show();
                            edtIdFoodCateGory.setText(autoIdFoodCateGory()+"");
                            edtNameFoodCateGory.setText("");
                            edtNameFoodCateGory.requestFocus();
                        }
                        else
                        {
                            Toast.makeText(AddFoodCateGoryActivity.this, "Thêm danh mục món ăn thất bại !", Toast.LENGTH_SHORT).show();
                            edtIdFoodCateGory.setText(autoIdFoodCateGory()+"");
                            edtNameFoodCateGory.setText("");
                            edtNameFoodCateGory.requestFocus();
                        }
                    }
                }
            }
        });
    }

    private void addControls()
    {
        edtIdFoodCateGory = (EditText) findViewById(R.id.edtIdFoodCateGory);
        edtNameFoodCateGory = (EditText) findViewById(R.id.edtNameFoodCateGory);
        btnAddFoodCateGory = (Button) findViewById(R.id.btnAddFoodCategory);
        edtIdFoodCateGory.setEnabled(false);
        edtIdFoodCateGory.setText(autoIdFoodCateGory()+"");
        edtNameFoodCateGory.requestFocus();
    }
    private int autoIdFoodCateGory()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        while (c.moveToNext())
            id = c.getInt(0);
        return id+1;
    }
    private boolean addFoodCateGory(int id,String name)
    {
        ContentValues value = new ContentValues();
        value.put("idFoodCateGory",id);
        value.put("nameFoodCateGory",name);
        long kq = LoginActivity.database.insert("FoodCateGory",null,value);
        if (kq>0)
            return true;
        return false;
    }
    private boolean isCheckExistNameFoodCateGory(String name)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        while(c.moveToNext())
        {
            if (c.getString(1).toLowerCase().equals(name.toLowerCase()))
                return true;
        }
        return false;
    }
}
