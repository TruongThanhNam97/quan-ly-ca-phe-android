package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.Toast;

import com.truongthanhnam.adapter.NhanVienAdapter;
import com.truongthanhnam.model.NhanVien;

public class QuanLyNhanVienActivity extends AppCompatActivity {

    TabHost tabHost;
    ListView lvThuNgan,lvPhaChe,lvNhanVien;
    NhanVienAdapter adapterThuNgan,adapterPhaChe,adapterNhanVien;
    public static NhanVien seletedNhanVien;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_nhan_vien);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        lvThuNgan.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                seletedNhanVien = adapterThuNgan.getItem(position);
                return false;
            }
        });
        lvPhaChe.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                seletedNhanVien = adapterPhaChe.getItem(position);
                return false;
            }
        });
        lvNhanVien.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                seletedNhanVien = adapterNhanVien.getItem(position);
                return false;
            }
        });
    }

    private void addControls()
    {
        tabHost = (TabHost) findViewById(R.id.tabHostNhanVien);
        tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("tab1");
        tab1.setContent(R.id.tab1);
        tab1.setIndicator("Thu ngân");
        tabHost.addTab(tab1);

        TabHost.TabSpec tab2 = tabHost.newTabSpec("tab2");
        tab2.setContent(R.id.tab2);
        tab2.setIndicator("Pha chế");
        tabHost.addTab(tab2);

        TabHost.TabSpec tab3 = tabHost.newTabSpec("tab3");
        tab3.setContent(R.id.tab3);
        tab3.setIndicator("Nhân viên");
        tabHost.addTab(tab3);

        lvThuNgan = (ListView) findViewById(R.id.lvThuNgan);
        adapterThuNgan = new NhanVienAdapter(QuanLyNhanVienActivity.this,R.layout.itemnhanvien);
        lvThuNgan.setAdapter(adapterThuNgan);
        lvPhaChe = (ListView) findViewById(R.id.lvPhaChe);
        adapterPhaChe = new NhanVienAdapter(QuanLyNhanVienActivity.this,R.layout.itemnhanvien);
        lvPhaChe.setAdapter(adapterPhaChe);
        lvNhanVien = (ListView) findViewById(R.id.lvNhanVien);
        adapterNhanVien = new NhanVienAdapter(QuanLyNhanVienActivity.this,R.layout.itemnhanvien);
        lvNhanVien.setAdapter(adapterNhanVien);

        registerForContextMenu(lvThuNgan);
        registerForContextMenu(lvPhaChe);
        registerForContextMenu(lvNhanVien);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionnhanvien,menu);
        MenuItem mnu = menu.findItem(R.id.searchNhanVien);
        SearchView searchNhanVien = (SearchView) mnu.getActionView();
        searchNhanVien.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    onResume();
                }
                else
                {
                    adapterThuNgan.clear();
                    adapterNhanVien.clear();
                    adapterPhaChe.clear();
                    Cursor c = LoginActivity.database.rawQuery("select * from NhanVien",null);
                    while (c.moveToNext())
                    {
                        if ((c.getString(1).toLowerCase().contains(newText.toLowerCase()) || c.getString(2).contains(newText) || c.getString(3).contains(newText)) && c.getInt(4)==0)
                        {
                            adapterThuNgan.add(new NhanVien(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getBlob(5)));
                        }
                        else if ((c.getString(1).toLowerCase().contains(newText.toLowerCase()) || c.getString(2).contains(newText) || c.getString(3).contains(newText)) && c.getInt(4)==1)
                        {
                            adapterPhaChe.add(new NhanVien(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getBlob(5)));
                        }
                        else if ((c.getString(1).toLowerCase().contains(newText.toLowerCase()) || c.getString(2).contains(newText) || c.getString(3).contains(newText)) && c.getInt(4)==2)
                        {
                            adapterNhanVien.add(new NhanVien(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getBlob(5)));
                        }
                    }
                    c.close();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuAddNhanVien)
        {
            Intent intent = new Intent(QuanLyNhanVienActivity.this,AddNhanVienActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextnhanvien,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuSuaNhanVien)
        {
            Intent intent = new Intent(QuanLyNhanVienActivity.this,EditNhanVienActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuXoaNhanVien)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(QuanLyNhanVienActivity.this);
            b.setIcon(android.R.drawable.ic_dialog_info);
            b.setMessage("Bạn có chắc muốn sa thải nhân viên "+seletedNhanVien.getNameNhanVien()+" hay không ?");
            b.setTitle("Hỏi xóa");
            b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LoginActivity.database.delete("NhanVien","idNhanVien=?",new String[]{seletedNhanVien.getIdNhanVien()+""});
                    Toast.makeText(QuanLyNhanVienActivity.this, "Sa thải nhân viên "+seletedNhanVien.getNameNhanVien()+" thành công", Toast.LENGTH_SHORT).show();
                    onResume();
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = b.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        addNhanVienThuNganForListView();
        addNhanVienPhaCheForListView();
        addNhanVienForListView();
    }

    private void addNhanVienForListView()
    {
        adapterNhanVien.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from NhanVien where idChucVu=?",new String[]{"2"});
        while (c.moveToNext())
            adapterNhanVien.add(new NhanVien(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getBlob(5)));
        c.close();
    }

    private void addNhanVienPhaCheForListView() {
        adapterPhaChe.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from NhanVien where idChucVu=?",new String[]{"1"});
        while (c.moveToNext())
            adapterPhaChe.add(new NhanVien(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getBlob(5)));
        c.close();
    }

    private void addNhanVienThuNganForListView()
    {
        adapterThuNgan.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from NhanVien where idChucVu=?",new String[]{"0"});
        while (c.moveToNext())
            adapterThuNgan.add(new NhanVien(c.getInt(0),c.getString(1),c.getString(2),c.getString(3),c.getInt(4),c.getBlob(5)));
        c.close();
    }
}
