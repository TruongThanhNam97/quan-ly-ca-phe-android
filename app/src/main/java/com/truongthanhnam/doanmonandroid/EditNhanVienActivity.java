package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.truongthanhnam.model.ChucVu;

import java.io.ByteArrayOutputStream;

public class EditNhanVienActivity extends AppCompatActivity {

    Spinner spinnerEditChucVu;
    ArrayAdapter<ChucVu> adapterChucVu;
    EditText edtEditNameNhanVien,edtEditPhoneNhanVien,edtEditCmndNhanVien;
    Button btnEditNhanVien;
    ChucVu seletedChucVu;
    ImageView imgEditAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_nhan_vien);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        imgEditAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,2);
            }
        });
        btnEditNhanVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEditNameNhanVien.getText().toString().isEmpty()||edtEditCmndNhanVien.getText().toString().isEmpty()||edtEditPhoneNhanVien.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(EditNhanVienActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Bạn chưa nhập đủ thông tin, vui long nhập lại !");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq = isExistPhoneAndCmnd(edtEditPhoneNhanVien.getText().toString().trim(),edtEditCmndNhanVien.getText().toString().trim());
                    if (kq && edtEditPhoneNhanVien.getText().toString().trim().equals(QuanLyNhanVienActivity.seletedNhanVien.getPhoneNhanVien())==false && edtEditCmndNhanVien.getText().toString().trim().equals(QuanLyNhanVienActivity.seletedNhanVien.getCmndNhanVien())==false)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(EditNhanVienActivity.this);
                        b.setTitle("Thông báo");
                        b.setMessage("Sô điện thoại hoặc số chứng minh dân đã bị trùng khớp, vui lòng nhập lại !");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        ContentValues value = new ContentValues();
                        value.put("nameNhanVien",edtEditNameNhanVien.getText().toString());
                        value.put("phoneNhanVien",edtEditPhoneNhanVien.getText().toString());
                        value.put("cmndNhanVien",edtEditCmndNhanVien.getText().toString());
                        value.put("idChucVu",seletedChucVu.getIdChucVu());
                        value.put("avatar",imageToByteArray());
                        LoginActivity.database.update("NhanVien",value,"idNhanVien=?",new String[]{QuanLyNhanVienActivity.seletedNhanVien.getIdNhanVien()+""});
                        Toast.makeText(EditNhanVienActivity.this, "Sửa thông tin nhân viên thành công", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        });
        spinnerEditChucVu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                seletedChucVu = adapterChucVu.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==2 && resultCode==RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imgEditAvatar.setImageBitmap(photo);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addControls()
    {
        spinnerEditChucVu = (Spinner) findViewById(R.id.spinnerEditChucVu);
        adapterChucVu = new ArrayAdapter<ChucVu>(EditNhanVienActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterChucVu.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerEditChucVu.setAdapter(adapterChucVu);
        edtEditNameNhanVien = (EditText) findViewById(R.id.edtEditNameNhanVien);
        edtEditPhoneNhanVien = (EditText) findViewById(R.id.edtEditPhoneNhanVien);
        edtEditCmndNhanVien = (EditText) findViewById(R.id.edtEditCmndNhanVien);
        btnEditNhanVien = (Button) findViewById(R.id.btnEditNhanVien);
        edtEditNameNhanVien.setText(QuanLyNhanVienActivity.seletedNhanVien.getNameNhanVien());
        edtEditPhoneNhanVien.setText(QuanLyNhanVienActivity.seletedNhanVien.getPhoneNhanVien());
        edtEditCmndNhanVien.setText(QuanLyNhanVienActivity.seletedNhanVien.getCmndNhanVien());
        imgEditAvatar = (ImageView) findViewById(R.id.imgEditAvatar);
        imgEditAvatar.setImageBitmap(byteArraytoBitMap(QuanLyNhanVienActivity.seletedNhanVien.getAvatar()));
    }
    private boolean isExistPhoneAndCmnd(String phone,String cmnd)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from NhanVien",null);
        while (c.moveToNext())
        {
            if (c.getString(2).equals(phone) || c.getString(3).equals(cmnd))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    @Override
    protected void onResume() {
        super.onResume();
        adapterChucVu.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from ChucVu",null);
        while (c.moveToNext())
        {
            if (c.getInt(0)==QuanLyNhanVienActivity.seletedNhanVien.getIdChucVu())
                adapterChucVu.add(new ChucVu(c.getInt(0),c.getString(1)));
        }
        c.close();
        Cursor cc = LoginActivity.database.rawQuery("select * from ChucVu",null);
        while (cc.moveToNext())
        {
            if (cc.getInt(0)!=QuanLyNhanVienActivity.seletedNhanVien.getIdChucVu())
                adapterChucVu.add(new ChucVu(cc.getInt(0),cc.getString(1)));
        }
        cc.close();
    }
    public byte[] imageToByteArray()
    {
        BitmapDrawable drawable = (BitmapDrawable) imgEditAvatar.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] b = stream.toByteArray();
        return b;
    }
    public Bitmap byteArraytoBitMap(byte[] b)
    {
        Bitmap bitmap = BitmapFactory.decodeByteArray(b,0,b.length);
        return bitmap;
    }
}
