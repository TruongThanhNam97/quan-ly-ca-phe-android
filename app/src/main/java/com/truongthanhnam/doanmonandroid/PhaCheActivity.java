package com.truongthanhnam.doanmonandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DTO.Order;
import com.truongthanhnam.adapter.TraMonAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PhaCheActivity extends AppCompatActivity {

    ListView lvDataToCook;
    TraMonAdapter adapter;
    public static Order seletedOrder;
    TimerTask timerTask;
    Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pha_che);
        addControls();
        addEvents();
    }

    private void addEvents() {
        lvDataToCook.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                seletedOrder = adapter.getItem(position);
                return false;
            }
        });
    }

    private void addControls() {
        lvDataToCook = (ListView) findViewById(R.id.lvDataToCook);
        adapter = new TraMonAdapter(PhaCheActivity.this,R.layout.itemtramon);
        lvDataToCook.setAdapter(adapter);
        registerForContextMenu(lvDataToCook);
    }

    @Override
    protected void onResume() {
        super.onResume();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayDataToCook();
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask,0,2000);
    }

    private void displayDataToCook()
    {
        ArrayList<Order> ds = new ArrayList<>();
        ds = OrderDAL.getListOrderToCook();
        adapter.clear();
        for(Order x : ds)
            adapter.add(x);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenutramon,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuXemMon)
        {
            Intent intent = new Intent(PhaCheActivity.this,XemMonActivity.class);
            startActivity(intent);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter1,R.anim.exist1);
    }
}
