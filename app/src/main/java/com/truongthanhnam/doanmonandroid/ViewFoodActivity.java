package com.truongthanhnam.doanmonandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.truongthanhnam.DAL.FooddDAL;
import com.truongthanhnam.adapter.ViewFoodAdapter;
import com.truongthanhnam.model.Food;
import com.truongthanhnam.model.FoodCateGory;

public class ViewFoodActivity extends AppCompatActivity {

    ListView lvViewFood;
    ViewFoodAdapter adapterFood;
    Spinner spinnerViewFoodCateGory;
    ArrayAdapter<FoodCateGory> adapterFoodCateGory;
    FoodCateGory selectedFoodCateGory;
    public static Food selectedFood;
    int x=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_food);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        spinnerViewFoodCateGory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFoodCateGory = adapterFoodCateGory.getItem(position);
                addFoodFromFoodCateGoryToListView(selectedFoodCateGory.getIdFoodCateGory());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lvViewFood.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedFood = adapterFood.getItem(position);
                return false;
            }
        });
    }

    private void addControls()
    {
        lvViewFood = (ListView) findViewById(R.id.lvViewFood);
        adapterFood = new ViewFoodAdapter(ViewFoodActivity.this,R.layout.itemviewfood);
        lvViewFood.setAdapter(adapterFood);
        spinnerViewFoodCateGory = (Spinner) findViewById(R.id.spinnerViewFoodCateGory);
        adapterFoodCateGory = new ArrayAdapter<FoodCateGory>(ViewFoodActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterFoodCateGory.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerViewFoodCateGory.setAdapter(adapterFoodCateGory);
        addFoodCateGoryToSpinner();
        //addFoodFromFoodCateGoryToListView(selectedFoodCateGory.getIdFoodCateGory());
        registerForContextMenu(lvViewFood);
    }

    private void addFoodCateGoryToSpinner()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from FoodCateGory",null);
        adapterFoodCateGory.clear();
        while (c.moveToNext())
            adapterFoodCateGory.add(new FoodCateGory(c.getInt(0),c.getString(1)));
        c.close();
    }

    private void addFoodFromFoodCateGoryToListView(int idFoodCateGory)
    {
        Cursor c = LoginActivity.database.query("Food",null,"idFoodCateGory=?",new String[]{idFoodCateGory+""},null,null,null);
        adapterFood.clear();
        while (c.moveToNext())
            adapterFood.add(new Food(c.getInt(0),c.getString(1),c.getInt(2),c.getDouble(3)));
        c.close();
        Animation a = AnimationUtils.loadAnimation(ViewFoodActivity.this,R.anim.scale);
        lvViewFood.setAnimation(a);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenuviewfood,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.menuEditFood)
        {
            Intent intent = new Intent(ViewFoodActivity.this,EditFoodActivity.class);
            startActivity(intent);
        }
        if (item.getItemId()==R.id.menuDeleteFood)
        {
            AlertDialog.Builder b = new AlertDialog.Builder(ViewFoodActivity.this);
            b.setTitle("Hỏi xóa");
            b.setMessage("Bạn có chắc muốn xóa "+selectedFood.getNameFood()+" ?");
            b.setIcon(android.R.drawable.ic_dialog_info);
            b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FooddDAL.deleteFooddByIdFoodd(selectedFood.getIdFood());
                    int kq = LoginActivity.database.delete("Food","idFood=?",new String[]{selectedFood.getIdFood()+""});
                    if (kq>0)
                    {
                        Toast.makeText(ViewFoodActivity.this, "Xóa "+selectedFood.getNameFood()+" thành công !", Toast.LENGTH_SHORT).show();
                        onResume();
                    }
                    else
                    {
                        Toast.makeText(ViewFoodActivity.this, "Xóa "+selectedFood.getNameFood()+" thất bại !", Toast.LENGTH_SHORT).show();
                        onResume();
                    }
                }
            });
            b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   dialog.dismiss();
                }
            });
            AlertDialog dialog = b.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onResume() {
        if (x==0)
        {
            x++;
        }
        else
        {
            addFoodFromFoodCateGoryToListView(selectedFoodCateGory.getIdFoodCateGory());
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenuviewfood,menu);
        MenuItem mnu = menu.findItem(R.id.menuSearchViewFood);
        SearchView searchView = (SearchView) mnu.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty())
                {
                    addFoodFromFoodCateGoryToListView(selectedFoodCateGory.getIdFoodCateGory());
                }
                else
                {
                    adapterFood.clear();
                    Cursor c = LoginActivity.database.query("Food",null,"nameFood like ? or price like ?",new String[] {"%"+newText+"%","%"+newText+"%"},null,null,null);
                    while (c.moveToNext())
                        adapterFood.add(new Food(c.getInt(0),c.getString(1),c.getInt(2),c.getDouble(3)));
                    c.close();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
