package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.truongthanhnam.model.ChucVu;

import java.io.ByteArrayOutputStream;

public class AddNhanVienActivity extends AppCompatActivity {

    Spinner spinnerChucVu;
    ArrayAdapter<ChucVu> adapterChucVu;
    EditText edtIdNhanVien,edtNameNhanVien,edtPhoneNhanVien,edtCmndNhanVien;
    Button btnAddNhanVien;
    ChucVu selectedChucVu;
    ImageView imgChupHinh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_nhan_vien);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        spinnerChucVu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedChucVu=adapterChucVu.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnAddNhanVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIdNhanVien.getText().toString().isEmpty() || edtNameNhanVien.getText().toString().isEmpty() || edtPhoneNhanVien.getText().toString().isEmpty() || edtCmndNhanVien.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(AddNhanVienActivity.this);
                    b.setMessage("Bạn chưa nhập đủ thông tin !");
                    b.setTitle("Thông báo");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq = isExitsPhoneAndCmnd(edtPhoneNhanVien.getText().toString().trim(),edtCmndNhanVien.getText().toString().trim());
                    if (kq)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(AddNhanVienActivity.this);
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setMessage("Số điện thoại hoặc số chứng minh nhân dân đã bị trùng, vui lòng nhập lại");
                        b.setTitle("Thông báo");
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        ContentValues value = new ContentValues();
                        value.put("idNhanVien",Integer.parseInt(edtIdNhanVien.getText().toString()));
                        value.put("nameNhanVien",edtNameNhanVien.getText().toString());
                        value.put("phoneNhanVien",edtPhoneNhanVien.getText().toString());
                        value.put("cmndNhanVien",edtCmndNhanVien.getText().toString());
                        value.put("idChucVu",selectedChucVu.getIdChucVu());
                        value.put("avatar",imageToByteArray());
                        LoginActivity.database.insert("NhanVien",null,value);
                        edtIdNhanVien.setText(autoIdNhanVien()+"");
                        edtNameNhanVien.setText("");
                        edtPhoneNhanVien.setText("");
                        edtCmndNhanVien.setText("");
                        imgChupHinh.setImageResource(R.drawable.camera);
                        edtNameNhanVien.requestFocus();
                        Toast.makeText(AddNhanVienActivity.this, "Thêm nhân viên thành công", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        imgChupHinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==1 && resultCode==RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imgChupHinh.setImageBitmap(photo);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addControls() {
        spinnerChucVu = (Spinner) findViewById(R.id.spinnerChucVu);
        adapterChucVu = new ArrayAdapter<ChucVu>(AddNhanVienActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterChucVu.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerChucVu.setAdapter(adapterChucVu);

        edtIdNhanVien = (EditText) findViewById(R.id.edtIdNhanVien);
        edtNameNhanVien = (EditText) findViewById(R.id.edtNameNhanVien);
        edtPhoneNhanVien = (EditText) findViewById(R.id.edtPhoneNhanVien);
        edtCmndNhanVien = (EditText) findViewById(R.id.edtCmndNhanVien);

        btnAddNhanVien = (Button) findViewById(R.id.btnAddNhanVien);

        edtIdNhanVien.setEnabled(false);
        edtIdNhanVien.setText(autoIdNhanVien()+"");

        imgChupHinh = (ImageView) findViewById(R.id.imgChupHinh);
    }
    private int autoIdNhanVien()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from NhanVien",null);
        while (c.moveToNext())
            id = c.getInt(0);
        c.close();
        return id+1;
    }
    private boolean isExitsPhoneAndCmnd(String phone,String cmnd)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from NhanVien",null);
        while (c.moveToNext())
        {
            if (c.getString(2).equals(phone) || c.getString(3).equals(cmnd))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    @Override
    protected void onResume() {
        super.onResume();
        adapterChucVu.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from chucVu",null);
        while (c.moveToNext())
            adapterChucVu.add(new ChucVu(c.getInt(0),c.getString(1)));
        c.close();
    }
    public byte[] imageToByteArray()
    {
        BitmapDrawable drawable = (BitmapDrawable) imgChupHinh.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] b = stream.toByteArray();
        return b;
    }
}
