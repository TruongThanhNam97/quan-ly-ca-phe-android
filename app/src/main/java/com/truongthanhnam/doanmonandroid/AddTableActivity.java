package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.truongthanhnam.DAL.TableeDAL;

public class AddTableActivity extends AppCompatActivity {

    EditText edtIdTable,edtNameTable;
    Button btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_table);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNameTable.getText().toString().isEmpty() || edtIdTable.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(AddTableActivity.this);
                    b.setMessage("Bạn chưa nhập đầy đủ thông tin !");
                    b.setTitle("Thông báo");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean kq = isExistNameTable();
                    if (kq)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(AddTableActivity.this);
                        b.setMessage("Tên bàn đã tồn tại rồi, vui lòng nhập lại !");
                        b.setTitle("Thông báo");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        TableeDAL.addTable(Integer.parseInt(edtIdTable.getText().toString()),edtNameTable.getText().toString());
                        boolean kq1 = addTable(Integer.parseInt(edtIdTable.getText().toString()), edtNameTable.getText().toString(), "Trống");
                        if (kq1)
                        {
                            Toast.makeText(AddTableActivity.this, "Thêm bàn thành công !", Toast.LENGTH_SHORT).show();
                            edtIdTable.setText(AutoIdTable() + "");
                            edtNameTable.setText("");
                            edtNameTable.requestFocus();
                        } else
                        {
                            Toast.makeText(AddTableActivity.this, "Thêm bàn thất bại", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private void addControls()
    {
        edtIdTable= (EditText) findViewById(R.id.edtIdTable);
        edtNameTable = (EditText) findViewById(R.id.edtNameTable);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        edtIdTable.setEnabled(false);
        edtIdTable.setText(AutoIdTable()+"");
        edtNameTable.requestFocus();
    }
    private int AutoIdTable()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        while (c.moveToNext())
        {
            id=c.getInt(0);
        }
        return id+1;
    }
    private boolean addTable(Integer idTable,String nameTable,String statusTable)
    {
        ContentValues value = new ContentValues();
        value.put("idTable",idTable);
        value.put("nameTable",nameTable);
        value.put("statusTable",statusTable);
        long kq = LoginActivity.database.insert("Ban",null,value);
        if (kq>0)
            return true;
        return false;
    }
    private boolean isExistNameTable()
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban",null);
        while (c.moveToNext())
        {
            if (edtNameTable.getText().toString().toLowerCase().equals(c.getString(1).toLowerCase()))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
}
