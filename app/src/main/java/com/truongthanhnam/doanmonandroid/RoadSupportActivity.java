package com.truongthanhnam.doanmonandroid;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.truongthanhnam.model.House;

public class RoadSupportActivity extends AppCompatActivity {

    Spinner spinnerFromHouse;
    Spinner spinnerToHouse;
    ArrayAdapter<House> adapterHouse;
    Button btnViewRod;
    House selectedFromHouse, selectedToHouse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_road_support);
        addControls();
        addEvents();
    }

    private void addEvents() {
        spinnerFromHouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFromHouse = adapterHouse.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerToHouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedToHouse = adapterHouse.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnViewRod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedFromHouse.getLatite()==selectedToHouse.getLatite() && selectedFromHouse.getLongtite()==selectedToHouse.getLongtite())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(RoadSupportActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Hai địa điểm trùng nhau, vui lòng chọn lại !");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.co.in/maps/dir/'"+selectedFromHouse.getLatite()+","+selectedFromHouse.getLongtite()+"'/'"+selectedToHouse.getLatite()+","+selectedToHouse.getLongtite()+"'/@10.8208986,106.6828623,18z/data=!3m1!4b1!4m9!4m8!1m3!2m2!1d"+selectedFromHouse.getLongtite()+"!2d"+selectedFromHouse.getLatite()+"!1m3!2m2!1d"+selectedToHouse.getLongtite()+"!2d"+selectedToHouse.getLatite()+""));
                    startActivity(intent);
                }
            }
        });
    }

    private void addControls() {
        spinnerFromHouse = (Spinner) findViewById(R.id.spinnerFromHouse);
        spinnerToHouse = (Spinner) findViewById(R.id.spinnerToHouse);
        adapterHouse = new ArrayAdapter<House>(RoadSupportActivity.this,android.R.layout.simple_spinner_dropdown_item);
        adapterHouse.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinnerFromHouse.setAdapter(adapterHouse);
        spinnerToHouse.setAdapter(adapterHouse);
        btnViewRod = (Button) findViewById(R.id.btnViewRoad);
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayData();
    }

    private void displayData() {
        adapterHouse.clear();
        Cursor c = LoginActivity.database.rawQuery("select * from House",null);
        while (c.moveToNext())
        {
            adapterHouse.add(new House(c.getInt(0),c.getString(1),c.getFloat(2),c.getFloat(3),c.getBlob(4)));
        }
        c.close();
    }
}
