package com.truongthanhnam.doanmonandroid;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.truongthanhnam.DAL.ConvertDAL;

public class AddHouseActivity extends AppCompatActivity {

    ImageView imgChupHouse;
    EditText edtIdHouse,edtNameHouse,edtViTuyen,edtKinhTuyen;
    Button btnAddHouse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_house);
        addControls();
        addEvents();
    }

    private void addEvents()
    {
        imgChupHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,1);
            }
        });
        btnAddHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtIdHouse.getText().toString().isEmpty()||edtNameHouse.getText().toString().isEmpty()||edtViTuyen.getText().toString().isEmpty()||edtKinhTuyen.getText().toString().isEmpty())
                {
                    AlertDialog.Builder b = new AlertDialog.Builder(AddHouseActivity.this);
                    b.setTitle("Thông báo");
                    b.setMessage("Bạn chưa nhập đủ thông tin, vui lòng nhập đầy đủ !");
                    b.setIcon(android.R.drawable.ic_dialog_info);
                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = b.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                else
                {
                    boolean check = isExistNameHouse(edtNameHouse.getText().toString().trim());
                    if (check)
                    {
                        AlertDialog.Builder b = new AlertDialog.Builder(AddHouseActivity.this);
                        b.setTitle("Thông báo");
                        b.setMessage("Tên nhà hàng đã tồn tại rồi, vui lòng đặt tên khác !");
                        b.setIcon(android.R.drawable.ic_dialog_info);
                        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = b.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                    else
                    {
                        BitmapDrawable b = (BitmapDrawable) imgChupHouse.getDrawable();
                        addHouse(Integer.parseInt(edtIdHouse.getText().toString()),edtNameHouse.getText().toString(),Float.parseFloat(edtViTuyen.getText().toString()),Float.parseFloat(edtKinhTuyen.getText().toString()),b);
                        Toast.makeText(AddHouseActivity.this, "Thêm nhà hàng thành công !", Toast.LENGTH_SHORT).show();
                        edtIdHouse.setText(autoIdHouse()+"");
                        edtNameHouse.setText("");
                        edtViTuyen.setText("");
                        edtKinhTuyen.setText("");
                        edtNameHouse.requestFocus();
                        imgChupHouse.setImageBitmap(null);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==1 && resultCode==RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imgChupHouse.setImageBitmap(photo);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addControls() {
        imgChupHouse = (ImageView) findViewById(R.id.imgChupHouse);
        edtIdHouse = (EditText) findViewById(R.id.edtIdHouse);
        edtNameHouse = (EditText) findViewById(R.id.edtNameHouse);
        edtViTuyen = (EditText) findViewById(R.id.edtViTuyen);
        edtKinhTuyen = (EditText) findViewById(R.id.edtKinhTuyen);
        btnAddHouse = (Button) findViewById(R.id.btnAddHouse);
        edtIdHouse.setText(autoIdHouse()+"");
        edtIdHouse.setEnabled(false);
        edtNameHouse.requestFocus();
    }
    private int autoIdHouse()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from House",null);
        while (c.moveToNext())
        {
            id = c.getInt(0);
        }
        c.close();
        return id+1;
    }
    private boolean isExistNameHouse(String nameHouse)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from House",null);
        while (c.moveToNext())
        {
            if (nameHouse.toLowerCase().trim().equals(c.getString(1).toLowerCase().trim()))
            {
                c.close();
                return true;
            }
        }
        c.close();
        return false;
    }
    private void addHouse(int idHouse, String nameHouse, float latite, float longtite, BitmapDrawable b)
    {
        ContentValues value = new ContentValues();
        value.put("idHouse",idHouse);
        value.put("nameHouse",nameHouse);
        value.put("latite",latite);
        value.put("longtite",longtite);
        value.put("avatarHouse", ConvertDAL.imageToByteArray(b));
        LoginActivity.database.insert("House",null,value);
    }
}
