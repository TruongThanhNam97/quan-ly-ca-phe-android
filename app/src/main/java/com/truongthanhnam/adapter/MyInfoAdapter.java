package com.truongthanhnam.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.truongthanhnam.DAL.ConvertDAL;
import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.House;

/**
 * Created by TruongThanh on 07-Oct-17.
 */

public class MyInfoAdapter implements GoogleMap.InfoWindowAdapter
{
    Activity context;
    House h;
    public  MyInfoAdapter(Activity context, House h)
    {
        this.context = context;
        this.h = h;
    }
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View customView = this.context.getLayoutInflater().inflate(R.layout.itemgoogle,null);
        ImageView imgDisplay = (ImageView) customView.findViewById(R.id.imgDisplayAvatarHouse);
        TextView txtDisplay = (TextView) customView.findViewById(R.id.txtDisplayNameHouse);
        imgDisplay.setImageBitmap(ConvertDAL.byteArrayToBitMap(h.getAvatarHouse()));
        txtDisplay.setText(h.getNameHouse());
        return customView;
    }
}
