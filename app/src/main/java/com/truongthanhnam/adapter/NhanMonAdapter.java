package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DAL.OrderInfoDAL;
import com.truongthanhnam.DAL.TableeDAL;
import com.truongthanhnam.DTO.Order;
import com.truongthanhnam.doanmonandroid.LoginActivity;
import com.truongthanhnam.doanmonandroid.R;

/**
 * Created by TruongThanh on 29-Sep-17.
 */

public class NhanMonAdapter extends ArrayAdapter<Order>
{
    Activity context;
    int resource;
    TextView txtNhanMon;
    ImageView imgNhanMon;
    public NhanMonAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtNhanMon = (TextView) customView.findViewById(R.id.txtNhanMon);
        imgNhanMon = (ImageView) customView.findViewById(R.id.imgNhanMon);
        Order o = getItem(position);
        txtNhanMon.setText(TableeDAL.getNameTableByIdTable(o.getIdTable()));
        imgNhanMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Xác nhận");
                b.setMessage("Đã phục vụ món ăn cho "+TableeDAL.getNameTableByIdTable(getItem(position).getIdTable())+" ?");
                b.setIcon(android.R.drawable.ic_dialog_info);
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        OrderInfoDAL.deleteOrderInfoByIdOrder(getItem(position).getIdOrder());
                        OrderDAL.updateStatusOrderByIdOrder(getItem(position).getIdOrder(),2);
                        remove(getItem(position));
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = b.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
        return customView;
    }
    public String getNameTableByIdTable(int idTable)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Ban where idTable=?",new String[]{idTable+""});
        String str = "";
        while (c.moveToNext())
        {
            str = c.getString(1);
        }
        c.close();
        return str;
    }
}
