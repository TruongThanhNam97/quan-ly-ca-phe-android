package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.FoodCateGory;

/**
 * Created by TruongThanh on 12-Sep-17.
 */

public class ViewFoodCateGoryAdapter extends ArrayAdapter<FoodCateGory>
{
    Activity context;
    int resource;
    TextView txtNameFoodCateGory;
    public ViewFoodCateGoryAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtNameFoodCateGory = (TextView) customView.findViewById(R.id.txtNameFoodCateGory);
        FoodCateGory f = getItem(position);
        txtNameFoodCateGory.setText(f.getNameFoodCateGory());
        return customView;
    }
}
