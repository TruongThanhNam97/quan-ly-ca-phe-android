package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.doanmonandroid.StatisticalActivity;
import com.truongthanhnam.model.Food;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by TruongThanh on 12-Sep-17.
 */

public class ViewFoodAdapter extends ArrayAdapter<Food>
{
    Activity context;
    int resource;
    TextView txtViewNameFood,txtViewPriceFood;

    public ViewFoodAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context=context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtViewNameFood = (TextView) customView.findViewById(R.id.txtViewNameFood);
        txtViewPriceFood = (TextView) customView.findViewById(R.id.txtViewPriceFood);
        Food f = getItem(position);
        txtViewNameFood.setText(f.getNameFood());
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtViewPriceFood.setText(df.format(f.getPrice())+" VNĐ");
        return customView;
    }
}
