package com.truongthanhnam.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.NhanVien;

/**
 * Created by TruongThanh on 23-Sep-17.
 */

public class NhanVienAdapter extends ArrayAdapter<NhanVien>
{
    Activity context;
    int resource;
    TextView txtNameNhanVien,txtPhoneNhanVien,txtCmnd;
    ImageView imgAvatarNhanVien,imgCall,imgSMS;
    public NhanVienAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtNameNhanVien = (TextView) customView.findViewById(R.id.txtNameNhanVien);
        txtPhoneNhanVien = (TextView) customView.findViewById(R.id.txtPhoneNhanVien);
        txtCmnd = (TextView) customView.findViewById(R.id.txtCmnd);
        imgAvatarNhanVien = (ImageView) customView.findViewById(R.id.imgAvatarNhanVien);
        imgCall = (ImageView) customView.findViewById(R.id.imgCall);
        imgSMS = (ImageView) customView.findViewById(R.id.imgSMS);
        NhanVien nv = getItem(position);
        txtNameNhanVien.setText(nv.getNameNhanVien());
        txtPhoneNhanVien.setText("Sđt :   "+nv.getPhoneNhanVien());
        txtCmnd.setText("Cmnd : "+nv.getCmndNhanVien());
        imgAvatarNhanVien.setImageBitmap(byteArraytoBitMap(nv.getAvatar()));
        imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Xác nhận gọi");
                b.setMessage("Bạn muốn gọi nhân viên "+getItem(position).getNameNhanVien()+" ?");
                b.setIcon(android.R.drawable.ic_dialog_info);
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+getItem(position).getPhoneNhanVien()));
                        context.startActivity(intent);
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = b.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
        imgSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Xác nhận nhắn tin");
                b.setMessage("Bạn muốn nhắn tin nhân viên "+getItem(position).getNameNhanVien()+" ?");
                b.setIcon(android.R.drawable.ic_dialog_info);
                b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Dialog d = new Dialog(context);
                        d.setContentView(R.layout.sms);
                        final EditText edtSMS = (EditText) d.findViewById(R.id.edtSMS);
                        Button btnSend = (Button) d.findViewById(R.id.btnSend);
                        btnSend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edtSMS.getText().toString().isEmpty())
                                {
                                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                                    b.setTitle("Thông báo");
                                    b.setMessage("Bạn chưa nhập tin nhắn !");
                                    b.setIcon(android.R.drawable.ic_dialog_info);
                                    b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog dialog = b.create();
                                    dialog.setCanceledOnTouchOutside(false);
                                    dialog.show();
                                }
                                else
                                {
                                    try
                                    {
                                        SmsManager smsManager = SmsManager.getDefault();
                                        smsManager.sendTextMessage(getItem(position).getPhoneNhanVien(), null,edtSMS.getText().toString(), null, null);
                                        Toast.makeText(context, "Tin nhắn gửi thành công", Toast.LENGTH_LONG).show();
                                    }

                                    catch (Exception e)
                                    {
                                        Toast.makeText(context, "Tin nhắn gửi thất bại", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        });
                        d.show();
                    }
                });
                b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = b.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
        return customView;
    }
    public Bitmap byteArraytoBitMap(byte[] b)
    {
        Bitmap bitmap = BitmapFactory.decodeByteArray(b,0,b.length);
        return bitmap;
    }
}
