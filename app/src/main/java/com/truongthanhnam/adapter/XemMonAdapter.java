package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.truongthanhnam.DAL.FooddDAL;
import com.truongthanhnam.DTO.OrderInfo;
import com.truongthanhnam.doanmonandroid.LoginActivity;
import com.truongthanhnam.doanmonandroid.R;

/**
 * Created by TruongThanh on 29-Sep-17.
 */

public class XemMonAdapter extends ArrayAdapter<OrderInfo>
{
    Activity context;
    int resource;
    TextView txtNameFoodOrderInfo,txtCountFoodOderInfo;
    public XemMonAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtNameFoodOrderInfo = (TextView) customView.findViewById(R.id.txtNameFoodOrderInfo);
        txtCountFoodOderInfo = (TextView) customView.findViewById(R.id.txtCountOrderInfo);
        OrderInfo o = getItem(position);
        txtNameFoodOrderInfo.setText(FooddDAL.getNameFooddByIdFoodd(o.getIdFoodd()));
        txtCountFoodOderInfo.setText(o.getCountOrderInfo()+"");
        return customView;
    }

    public String getNameFoodByIdFood(int idFood)
    {
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood=?",new String[]{idFood+""});
        String str="";
        while (c.moveToNext())
        {
            str = c.getString(1);
        }
        c.close();
        return str;
    }
}
