package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.truongthanhnam.doanmonandroid.LoginActivity;
import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.BillInfo;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by TruongThanh on 13-Sep-17.
 */

public class BillInfoAdapter extends ArrayAdapter<BillInfo>
{
    Activity context;
    int resource;
    TextView txtNameFoodBillInfo,txtPriceFoodBillInfo,txtCountFoodBillInfo;
    public BillInfoAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context=context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtNameFoodBillInfo = (TextView) customView.findViewById(R.id.txtNameFoodBillInfo);
        txtPriceFoodBillInfo = (TextView) customView.findViewById(R.id.txtPriceFoodBillInfo);
        txtCountFoodBillInfo = (TextView) customView.findViewById(R.id.txtCountFoodBillInfo);
        BillInfo b = getItem(position);
        txtNameFoodBillInfo.setText(getNameFoodByIdFood(b.getIdFood()));
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtPriceFoodBillInfo.setText(df.format(getPriceFoodByIdFood(b.getIdFood()))+" VNĐ");
        txtCountFoodBillInfo.setText(b.getCount()+"");
        return customView;
    }
    private String getNameFoodByIdFood(int id)
    {
        String str="";
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            str = c.getString(1);
        c.close();
        return str;
    }
    private double getPriceFoodByIdFood(int id)
    {
        double price=0.0;
        Cursor c = LoginActivity.database.rawQuery("select * from Food where idFood="+id+"",null);
        while (c.moveToNext())
            price = c.getDouble(3);
        c.close();
        return price;
    }
}
