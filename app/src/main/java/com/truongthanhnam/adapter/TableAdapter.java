package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.Table;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class TableAdapter extends ArrayAdapter<Table>
{
    Activity context;
    int resource;
    TextView txtTenBanAn;
    public TableAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtTenBanAn = (TextView) customView.findViewById(R.id.txtTenBanAn);
        Table table = getItem(position);
        txtTenBanAn.setText(table.getNameTable()+" - "+table.getStatusTable());
        return customView;
    }
}
