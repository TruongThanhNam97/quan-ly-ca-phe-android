package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.truongthanhnam.DAL.ConvertDAL;
import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.House;

/**
 * Created by TruongThanh on 06-Oct-17.
 */

public class HouseAdapter extends ArrayAdapter<House>
{
    Activity context;
    int resource;
    ImageView imgAvatarHouse;
    TextView txtNameHouse;
    public HouseAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        imgAvatarHouse = (ImageView) customView.findViewById(R.id.imgAvatarHouse);
        txtNameHouse = (TextView) customView.findViewById(R.id.txtNameHouse);
        House h = getItem(position);
        imgAvatarHouse.setImageBitmap(ConvertDAL.byteArrayToBitMap(h.getAvatarHouse()));
        txtNameHouse.setText(h.getNameHouse());
        return customView;
    }
}
