package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.truongthanhnam.DAL.OrderDAL;
import com.truongthanhnam.DAL.OrderInfoDAL;
import com.truongthanhnam.doanmonandroid.LoginActivity;
import com.truongthanhnam.doanmonandroid.MenuActivity;
import com.truongthanhnam.doanmonandroid.OrderActivity;
import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.Food;
import com.truongthanhnam.model.Table;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by TruongThanh on 12-Sep-17.
 */

public class OrderFoodAdapter extends ArrayAdapter<Food>
{
    Activity context;
    int resource;
    TextView txtNameOrderFood,txtPriceFood;
    ImageView imgUp,imgDown,imgOrderFood;
    int x =0;
    int idBill=0;

    public OrderFoodAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context=context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtNameOrderFood = (TextView) customView.findViewById(R.id.txtNameFoodOrder);
        txtPriceFood= (TextView) customView.findViewById(R.id.txtPriceFood);
        imgUp = (ImageView) customView.findViewById(R.id.imgUp);
        imgDown = (ImageView) customView.findViewById(R.id.imgDown);
        imgOrderFood = (ImageView) customView.findViewById(R.id.imgOrder);
        Food f = getItem(position);
        txtNameOrderFood.setText(f.getNameFood());
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtPriceFood.setText(df.format(f.getPrice())+" VNĐ");
        imgUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(OrderActivity.txtCountFood.getText().toString());
                count++;
                OrderActivity.txtCountFood.setText(count+"");
                OrderActivity.txtDisplayCountFood.setText(getItem(position).getNameFood());
            }
        });
        imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(OrderActivity.txtCountFood.getText().toString());
                count--;
                if (count==1)
                {
                    OrderActivity.txtCountFood.setText(count+"");
                    OrderActivity.txtDisplayCountFood.setText("Count:");
                }
                else if (count<1)
                {
                    count=1;
                    OrderActivity.txtCountFood.setText(count+"");
                    OrderActivity.txtDisplayCountFood.setText("Count:");
                }
                else
                {
                    OrderActivity.txtCountFood.setText(count+"");
                    OrderActivity.txtDisplayCountFood.setText(getItem(position).getNameFood());
                }
            }
        });
        imgOrderFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MenuActivity.selectedTable.getStatusTable().equals("Trống") && x==0)
                {
                    x++;
                    updateStatusTable();
                    createBillOfTable();
                    createBillinfoOfBill(position,Integer.parseInt(OrderActivity.txtCountFood.getText().toString()));
                    OrderActivity.txtCountFood.setText(1+"");
                    OrderActivity.txtDisplayCountFood.setText("Count:");
                    Toast.makeText(context,getItem(position).getNameFood()+" đã được thêm vào hóa đơn", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    createBillinfoOfBill(position,Integer.parseInt(OrderActivity.txtCountFood.getText().toString()));
                    OrderActivity.txtCountFood.setText(1+"");
                    OrderActivity.txtDisplayCountFood.setText("Count:");
                    Toast.makeText(context,getItem(position).getNameFood()+" đã được thêm vào hóa đơn", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return customView;
    }

    private void createBillinfoOfBill(int position,int count)
    {
        int z=0;
        idBill = getIdBillByIdTableAndStatusBill();
        Cursor c = LoginActivity.database.rawQuery("select * from BillInfo where idBill="+idBill+"",null);
        while (c.moveToNext())
        {
            if (c.getInt(2)==getItem(position).getIdFood())
            {
                boolean kq = OrderInfoDAL.isExistOrderInfoByIdOrderAndIdFood(getItem(position).getIdFood(),idBill);
                if (kq)
                {
                    OrderInfoDAL.updateCountOrderInfoInOrderInfo(OrderInfoDAL.getIdBillInfoByIdOrderAndIdFood(getItem(position).getIdFood(),idBill),OrderInfoDAL.getCountOrderInfoByIdOrderAmdIdFood(getItem(position).getIdFood(),idBill)+count);
                }
                else
                {
                    OrderInfoDAL.addOrderInfo(OrderInfoDAL.autoIdOrderInfo(),getIdBillByIdTableAndStatusBill(),getItem(position).getIdFood(),count);
                }
                ContentValues value = new ContentValues();
                value.put("count",c.getInt(3)+count);
                LoginActivity.database.update("BillInfo",value,"idBillInfo=?",new String[]{c.getInt(0)+""});
                z++;
            }
        }
        c.close();
        if (z!=0)
            return;
        int id = autoIdBillInfo();
        OrderInfoDAL.addOrderInfo(OrderInfoDAL.autoIdOrderInfo(),getIdBillByIdTableAndStatusBill(),getItem(position).getIdFood(),count);
        ContentValues value = new ContentValues();
        value.put("idBillInfo",id);
        value.put("idBill",idBill);
        value.put("idFood",getItem(position).getIdFood());
        value.put("count",count);
        LoginActivity.database.insert("BillInfo",null,value);
    }

    private void createBillOfTable()
    {
        int id = autoIdBill();
        OrderDAL.addOrder(id,MenuActivity.selectedTable.getIdTable(),2);
        idBill = autoIdBill();
        ContentValues value = new ContentValues();
        value.put("idBill",id);
        value.put("idTable",MenuActivity.selectedTable.getIdTable());
        value.put("statusBill",0);
        value.put("totalPrice",0.0);
        LoginActivity.database.insert("Bill",null,value);
    }

    private void updateStatusTable()
    {
        ContentValues value = new ContentValues();
        value.put("statusTable","Có người");
        LoginActivity.database.update("Ban",value,"idTable=?",new String[]{MenuActivity.selectedTable.getIdTable()+""});
    }
    private int autoIdBill()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from Bill",null);
        while (c.moveToNext())
            id=c.getInt(0);
        c.close();
        return id+1;
    }
    private int autoIdBillInfo()
    {
        int id=0;
        Cursor c = LoginActivity.database.rawQuery("select * from BillInfo",null);
        while (c.moveToNext())
            id =c.getInt(0);
        c.close();
        return id+1;
    }
    private int getIdBillByIdTableAndStatusBill()
    {
        int id=0;
        Cursor c = LoginActivity.database.query("Bill",null,"idTable=? and statusBill=?",new String[]{MenuActivity.selectedTable.getIdTable()+"",0+""},null,null,null);
        while (c.moveToNext())
            id = c.getInt(0);
        c.close();
        return id;
    }
}
