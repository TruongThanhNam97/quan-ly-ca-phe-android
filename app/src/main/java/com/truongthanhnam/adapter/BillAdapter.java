package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.Bill;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Created by TruongThanh on 14-Sep-17.
 */

public class BillAdapter extends ArrayAdapter<Bill>
{
    Activity context;
    int resource;
    TextView txtPriceOfBill;
    public BillAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        txtPriceOfBill = (TextView) customView.findViewById(R.id.txtPriceOfBill);
        Bill b = getItem(position);
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        txtPriceOfBill.setText(df.format(b.getTotalPrice())+" VNĐ");
        return customView;
    }
}
