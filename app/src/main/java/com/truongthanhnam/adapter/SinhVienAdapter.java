package com.truongthanhnam.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.truongthanhnam.doanmonandroid.R;
import com.truongthanhnam.model.SinhVien;

import java.net.URL;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class SinhVienAdapter extends ArrayAdapter<SinhVien>
{
    Activity context;
    int resource;
    ImageView imgAvatar;
    TextView txtHoTenSinhVien,txtMaSoSinhVien;
    public SinhVienAdapter(@NonNull Activity context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View customView = this.context.getLayoutInflater().inflate(this.resource,null);
        imgAvatar = (ImageView) customView.findViewById(R.id.imgAvatar);
        txtHoTenSinhVien = (TextView) customView.findViewById(R.id.txtHoTenSinhVien);
        txtMaSoSinhVien = (TextView) customView.findViewById(R.id.txtMaSoSinhVien);
        SinhVien sv = getItem(position);
        txtHoTenSinhVien.setText(sv.getName());
        txtMaSoSinhVien.setText(sv.getMssv());
        if (sv.getName().equals("Trương Thanh Nam"))
        {
           imgAvatar.setImageResource(R.drawable.truongthanhnam);
        }
        else if (sv.getName().equals("Đặng Xuân Duy Khương"))
        {
            imgAvatar.setImageResource(R.drawable.khuong);
        }
        else if (sv.getName().equals("Nguyễn Hoàng Phúc"))
        {
            imgAvatar.setImageResource(R.drawable.phuc);
        }
        return customView;
    }
}
