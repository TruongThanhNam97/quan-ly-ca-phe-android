package com.truongthanhnam.model;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 23-Sep-17.
 */

public class ChucVu
{
    private int idChucVu;
    private String tenChucVu;
    private ArrayList<NhanVien> nhanViens = new ArrayList<>();

    public ChucVu() {
    }

    public ChucVu(int idChucVu, String tenChucVu) {
        this.idChucVu = idChucVu;
        this.tenChucVu = tenChucVu;
    }

    public int getIdChucVu() {
        return idChucVu;
    }

    public void setIdChucVu(int idChucVu) {
        this.idChucVu = idChucVu;
    }

    public String getTenChucVu() {
        return tenChucVu;
    }

    public void setTenChucVu(String tenChucVu) {
        this.tenChucVu = tenChucVu;
    }

    @Override
    public String toString() {
        return this.tenChucVu;
    }
}
