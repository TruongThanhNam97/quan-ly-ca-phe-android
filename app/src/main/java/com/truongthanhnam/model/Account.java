package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class Account
{
    private String name;
    private String passWord;

    public Account() {
    }

    public Account(String name, String passWord) {
        this.name = name;
        this.passWord = passWord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
