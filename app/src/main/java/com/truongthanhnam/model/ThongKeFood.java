package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 14-Sep-17.
 */

public class ThongKeFood
{
    private int idFood;
    private int count;

    public ThongKeFood() {
    }

    public ThongKeFood(int idFood, int count) {
        this.idFood = idFood;
        this.count = count;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
