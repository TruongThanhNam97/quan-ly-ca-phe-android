package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class SinhVien
{
    private String name;
    private String mssv;

    public SinhVien() {
    }

    public SinhVien(String name, String mssv) {
        this.name = name;
        this.mssv = mssv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }
}
