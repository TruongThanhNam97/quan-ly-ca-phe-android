package com.truongthanhnam.model;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class Bill
{
    private int idBill;

    private  Table table;
    private int idTable;

    private int statusBill;  //0 chua thanh toan , 1 da thanh toan
    private double totalPrice;

    private ArrayList<BillInfo> billInfos = new ArrayList<>();

    public Bill() {
    }

    public Bill(int idBill, int idTable, int statusBill, double totalPrice) {
        this.idBill = idBill;
        this.idTable = idTable;
        this.statusBill = statusBill;
        this.totalPrice = totalPrice;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public int getStatusBill() {
        return statusBill;
    }

    public void setStatusBill(int statusBill) {
        this.statusBill = statusBill;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public ArrayList<BillInfo> getBillInfos() {
        return billInfos;
    }

    public void setBillInfos(ArrayList<BillInfo> billInfos) {
        this.billInfos = billInfos;
    }
}
