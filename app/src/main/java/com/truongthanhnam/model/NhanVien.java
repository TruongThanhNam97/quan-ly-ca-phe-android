package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 23-Sep-17.
 */

public class NhanVien
{
    private int idNhanVien;
    private String nameNhanVien;
    private String phoneNhanVien;
    private String cmndNhanVien;
    private ChucVu chucVu;
    private int idChucVu;
    private byte[] avatar;

    public NhanVien() {
    }

    public NhanVien(int idNhanVien, String nameNhanVien, String phoneNhanVien, String cmndNhanVien, int idChucVu, byte[] avatar) {
        this.idNhanVien = idNhanVien;
        this.nameNhanVien = nameNhanVien;
        this.phoneNhanVien = phoneNhanVien;
        this.cmndNhanVien = cmndNhanVien;
        this.idChucVu = idChucVu;
        this.avatar = avatar;
    }

    public int getIdNhanVien() {
        return idNhanVien;
    }

    public void setIdNhanVien(int idNhanVien) {
        this.idNhanVien = idNhanVien;
    }

    public String getNameNhanVien() {
        return nameNhanVien;
    }

    public void setNameNhanVien(String nameNhanVien) {
        this.nameNhanVien = nameNhanVien;
    }

    public String getPhoneNhanVien() {
        return phoneNhanVien;
    }

    public void setPhoneNhanVien(String phoneNhanVien) {
        this.phoneNhanVien = phoneNhanVien;
    }

    public String getCmndNhanVien() {
        return cmndNhanVien;
    }

    public void setCmndNhanVien(String cmndNhanVien) {
        this.cmndNhanVien = cmndNhanVien;
    }

    public int getIdChucVu() {
        return idChucVu;
    }

    public void setIdChucVu(int idChucVu) {
        this.idChucVu = idChucVu;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }
}
