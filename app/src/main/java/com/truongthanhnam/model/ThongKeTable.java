package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 15-Sep-17.
 */

public class ThongKeTable
{
    private int idTable;
    private int count;

    public ThongKeTable() {
    }

    public ThongKeTable(int idTable, int count) {
        this.idTable = idTable;
        this.count = count;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
