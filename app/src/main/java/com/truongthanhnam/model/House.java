package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 06-Oct-17.
 */

public class House
{
    private int idHouse;
    private String nameHouse;
    private float latite;
    private float longtite;
    private byte[] avatarHouse;

    public House() {
    }

    public House(int idHouse, String nameHouse, float latite, float longtite, byte[] avartarHouse) {
        this.idHouse = idHouse;
        this.nameHouse = nameHouse;
        this.latite = latite;
        this.longtite = longtite;
        this.avatarHouse = avartarHouse;
    }

    public int getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(int idHouse) {
        this.idHouse = idHouse;
    }

    public String getNameHouse() {
        return nameHouse;
    }

    public void setNameHouse(String nameHouse) {
        this.nameHouse = nameHouse;
    }

    public float getLatite() {
        return latite;
    }

    public void setLatite(float latite) {
        this.latite = latite;
    }

    public float getLongtite() {
        return longtite;
    }

    public void setLongtite(float longtite) {
        this.longtite = longtite;
    }

    public byte[] getAvatarHouse() {
        return avatarHouse;
    }

    public void setAvatarHouse(byte[] avatarHouse) {
        this.avatarHouse = avatarHouse;
    }

    @Override
    public String toString() {
        return nameHouse;
    }
}
