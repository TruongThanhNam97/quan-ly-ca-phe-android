package com.truongthanhnam.model;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class FoodCateGory
{
    private int idFoodCateGory;
    private String nameFoodCateGory;
    private ArrayList<Food> foods = new ArrayList<>();

    public FoodCateGory() {
    }

    public FoodCateGory(int idFoodCateGory, String nameFoodCateGory) {
        this.idFoodCateGory = idFoodCateGory;
        this.nameFoodCateGory = nameFoodCateGory;
    }

    public int getIdFoodCateGory() {
        return idFoodCateGory;
    }

    public void setIdFoodCateGory(int idFoodCateGory) {
        this.idFoodCateGory = idFoodCateGory;
    }

    public String getNameFoodCateGory() {
        return nameFoodCateGory;
    }

    public void setNameFoodCateGory(String nameFoodCateGory) {
        this.nameFoodCateGory = nameFoodCateGory;
    }

    public ArrayList<Food> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<Food> foods) {
        this.foods = foods;
    }

    @Override
    public String toString() {
        return this.nameFoodCateGory;
    }
}
