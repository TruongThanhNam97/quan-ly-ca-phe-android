package com.truongthanhnam.model;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class Food
{
    private int idFood;
    private String nameFood;

    private  FoodCateGory foodCateGory;
    private int idFoodCateGory;

    private double price;
    private ArrayList<BillInfo> billInfos = new ArrayList<>();

    public Food() {
    }

    public Food(int idFood, String nameFood, int idFoodCateGory, double price) {
        this.idFood = idFood;
        this.nameFood = nameFood;
        this.idFoodCateGory = idFoodCateGory;
        this.price = price;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public String getNameFood() {
        return nameFood;
    }

    public void setNameFood(String nameFood) {
        this.nameFood = nameFood;
    }

    public FoodCateGory getFoodCateGory() {
        return foodCateGory;
    }

    public void setFoodCateGory(FoodCateGory foodCateGory) {
        this.foodCateGory = foodCateGory;
    }

    public int getIdFoodCateGory() {
        return idFoodCateGory;
    }

    public void setIdFoodCateGory(int idFoodCateGory) {
        this.idFoodCateGory = idFoodCateGory;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<BillInfo> getBillInfos() {
        return billInfos;
    }

    public void setBillInfos(ArrayList<BillInfo> billInfos) {
        this.billInfos = billInfos;
    }
}
