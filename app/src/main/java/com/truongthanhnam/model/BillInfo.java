package com.truongthanhnam.model;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class BillInfo
{
    private int idBillInfo;

    private Bill bill;
    private int idBill;

    private Food food;
    private int idFood;

    private int count;

    public BillInfo() {
    }

    public BillInfo(int idBillInfo, int idBill, int idFood, int count) {
        this.idBillInfo = idBillInfo;
        this.idBill = idBill;
        this.idFood = idFood;
        this.count = count;
    }

    public int getIdBillInfo() {
        return idBillInfo;
    }

    public void setIdBillInfo(int idBillInfo) {
        this.idBillInfo = idBillInfo;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
