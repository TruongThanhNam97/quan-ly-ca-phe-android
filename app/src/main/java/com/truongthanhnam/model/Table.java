package com.truongthanhnam.model;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 11-Sep-17.
 */

public class Table
{
    private int idTable;
    private String nameTable;
    private String statusTable;
    private ArrayList<Bill> bills = new ArrayList<>();

    public Table() {
    }

    public Table(int idTable, String nameTable, String statusTable) {
        this.idTable = idTable;
        this.nameTable = nameTable;
        this.statusTable = statusTable;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public String getNameTable() {
        return nameTable;
    }

    public void setNameTable(String nameTable) {
        this.nameTable = nameTable;
    }

    public String getStatusTable() {
        return statusTable;
    }

    public void setStatusTable(String statusTable) {
        this.statusTable = statusTable;
    }

    public ArrayList<Bill> getBills() {
        return bills;
    }

    public void setBills(ArrayList<Bill> bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return this.nameTable;
    }
}
