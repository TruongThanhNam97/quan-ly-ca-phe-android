package com.truongthanhnam.DTO;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class OrderInfo
{
    private int idOrderInfo;
    private Order order;
    private int idOrder;
    private Foodd foodd;
    private int idFoodd;
    private int countOrderInfo;

    public OrderInfo() {
    }

    public OrderInfo(int idOrderInfo, int idOrder, int idFoodd, int countOrderInfo) {
        this.idOrderInfo = idOrderInfo;
        this.idOrder = idOrder;
        this.idFoodd = idFoodd;
        this.countOrderInfo = countOrderInfo;
    }

    public int getIdOrderInfo() {
        return idOrderInfo;
    }

    public void setIdOrderInfo(int idOrderInfo) {
        this.idOrderInfo = idOrderInfo;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdFoodd() {
        return idFoodd;
    }

    public void setIdFoodd(int idFoodd) {
        this.idFoodd = idFoodd;
    }

    public int getCountOrderInfo() {
        return countOrderInfo;
    }

    public void setCountOrderInfo(int countOrderInfo) {
        this.countOrderInfo = countOrderInfo;
    }
}
