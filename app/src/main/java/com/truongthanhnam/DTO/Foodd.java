package com.truongthanhnam.DTO;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class Foodd
{
    private int idFoodd;
    private String nameFoodd;
    private ArrayList<OrderInfo> orderInfos = new ArrayList<>();

    public Foodd() {
    }

    public Foodd(int idFoodd, String nameFoodd) {
        this.idFoodd = idFoodd;
        this.nameFoodd = nameFoodd;
    }

    public int getIdFoodd() {
        return idFoodd;
    }

    public void setIdFoodd(int idFoodd) {
        this.idFoodd = idFoodd;
    }

    public String getNameFoodd() {
        return nameFoodd;
    }

    public void setNameFoodd(String nameFoodd) {
        this.nameFoodd = nameFoodd;
    }

    public ArrayList<OrderInfo> getOrderInfos() {
        return orderInfos;
    }

    public void setOrderInfos(ArrayList<OrderInfo> orderInfos) {
        this.orderInfos = orderInfos;
    }
}
