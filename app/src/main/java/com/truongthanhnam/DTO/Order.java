package com.truongthanhnam.DTO;

import com.truongthanhnam.model.Table;

import java.util.ArrayList;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class Order
{
    private int idOrder;

    private Table table;
    private int idTable;

    private int statusOrder;

    private ArrayList<OrderInfo> orderInfos = new ArrayList<>();

    public Order() {
    }

    public Order(int idOrder, int idTable, int statusOrder) {
        this.idOrder = idOrder;
        this.idTable = idTable;
        this.statusOrder = statusOrder;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public int getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(int statusOrder) {
        this.statusOrder = statusOrder;
    }

    public ArrayList<OrderInfo> getOrderInfos() {
        return orderInfos;
    }

    public void setOrderInfos(ArrayList<OrderInfo> orderInfos) {
        this.orderInfos = orderInfos;
    }
}
