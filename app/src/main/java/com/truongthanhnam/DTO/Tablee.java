package com.truongthanhnam.DTO;

/**
 * Created by TruongThanh on 03-Oct-17.
 */

public class Tablee
{
    private int idTablee;
    private String nameTablee;

    public Tablee() {
    }

    public Tablee(int idTablee, String nameTablee) {
        this.idTablee = idTablee;
        this.nameTablee = nameTablee;
    }

    public int getIdTablee() {
        return idTablee;
    }

    public void setIdTablee(int idTablee) {
        this.idTablee = idTablee;
    }

    public String getNameTablee() {
        return nameTablee;
    }

    public void setNameTablee(String nameTablee) {
        this.nameTablee = nameTablee;
    }
}
