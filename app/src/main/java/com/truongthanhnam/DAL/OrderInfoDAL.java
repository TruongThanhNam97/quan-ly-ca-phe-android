package com.truongthanhnam.DAL;

import android.util.Log;

import com.truongthanhnam.DTO.OrderInfo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class OrderInfoDAL
{
    public static ArrayList<OrderInfo> getListOrderInfoByIdOrder(int idOrder)
    {
        ArrayList<OrderInfo> orderInfos = new ArrayList<>();
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from OrderInfo where idOrder="+idOrder+"";
            orderInfos.clear();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            while (rs.next())
                orderInfos.add(new OrderInfo(rs.getInt("idOrderInfo"), rs.getInt("idOrder"), rs.getInt("idFoodd"), rs.getInt("countOrderInfo")));
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return orderInfos;
    }
    public static void addOrderInfo(int idOrderInfo,int idOrder,int idFoodd,int countOrderInfo)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "insert into OrderInfo values('"+idOrderInfo+"','"+idOrder+"','"+idFoodd+"','"+countOrderInfo+"')";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next()==true)
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void updateCountOrderInfoInOrderInfo(int idOrderInfo,int countOrderInfo)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "update OrderInfo set countOrderInfo="+countOrderInfo+" where idOrderInfo="+idOrderInfo+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void deleteOrderInfo(int idOrderInfo)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from OrderInfo where idOrderInfo="+idOrderInfo+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void deleteOrderInfoByIdOrder(int idOrder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from OrderInfo where idOrder="+idOrder+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static int autoIdOrderInfo()
    {
        int id=0;
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from OrderInfo";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            while (rs.next())
            {
                id = rs.getInt("idOrderInfo");
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return id+1;
    }
    public static void updateIdOrder(int idOrderInfo,int idOrder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "update OrderInfo set idOrder="+idOrder+" where idOrderInfo="+idOrderInfo+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static boolean isExistOrderInfoByIdOrderAndIdFood(int idFoodd,int idOrder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from OrderInfo where idOrder="+idOrder+" and idFoodd="+idFoodd+"";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            if (rs.next())
                return true;
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return false;
    }
    public static int getIdBillInfoByIdOrderAndIdFood(int idFoodd,int idOrder)
    {
        int id=0;
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from OrderInfo where idOrder="+idOrder+" and idFoodd="+idFoodd+"";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            if (rs.next())
                id = rs.getInt("idOrderInfo");
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return id;
    }
    public static int getCountOrderInfoByIdOrderAmdIdFood(int idFoodd,int idOrder)
    {
        int count=0;
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from OrderInfo where idOrder="+idOrder+" and idFoodd="+idFoodd+"";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            if (rs.next())
                count = rs.getInt("countOrderInfo");
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return count;
    }
    public static void deleteOrderInfoByIdOrderAndIdFood(int idFoodd,int idOrder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from OrderInfo where idOrder="+idOrder+" and idFoodd="+idFoodd+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
}
