package com.truongthanhnam.DAL;

import android.util.Log;

import com.truongthanhnam.DTO.Order;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class OrderDAL
{
    public static ArrayList<Order> getListOrderToCook()
    {
        ArrayList<Order> orders = new ArrayList<>();
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Orderr where statusOrder=0";
            orders.clear();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            while (rs.next())
                orders.add(new Order(rs.getInt("idOrder"),rs.getInt("idTable"),rs.getInt("statusOrder")));
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return orders;
    }
    public static ArrayList<Order> getListOrderToServe()
    {
        ArrayList<Order> orders = new ArrayList<>();
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Orderr where statusOrder=1";
            orders.clear();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            while (rs.next())
                orders.add(new Order(rs.getInt("idOrder"),rs.getInt("idTable"),rs.getInt("statusOrder")));
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return orders;
    }
    public static void addOrder(int idOrder,int idTable,int statusOder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "insert into Orderr values('"+idOrder+"','"+idTable+"','"+statusOder+"')";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if(rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void updateStatusOrderByIdOrder(int idOrder,int statusOrder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "update Orderr set statusOrder="+statusOrder+" where idOrder="+idOrder+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if(rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void deleteOrder(int idOrder)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from Orderr where idOrder="+idOrder+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if(rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void deleteOrderByIdTable(int idTable)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from Orderr where idTable="+idTable+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if(rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static int autoIdOrder()
    {
        int id=0;
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Orderr";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                while (rs.next())
                {
                    id = rs.getInt("idOrder");
                    check1=false;
                }
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return id+1;
    }
    public static void updateIdTable(int idOrder,int idIdTableTo)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "update Orderr set idTable="+idIdTableTo+" where idOrder="+idOrder+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
}
