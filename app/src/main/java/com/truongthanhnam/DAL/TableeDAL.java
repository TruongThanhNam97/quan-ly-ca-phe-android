package com.truongthanhnam.DAL;

import android.util.Log;

import com.truongthanhnam.DTO.Tablee;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by TruongThanh on 03-Oct-17.
 */

public class TableeDAL
{
    public static void addTable(int idTablee,String nameTablee)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "insert into Tablee values('"+idTablee+"','"+nameTablee+"')";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void editTable(int idTablee,String nameTablee)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "update Tablee set nameTablee='"+nameTablee+"' where idTablee="+idTablee+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void deleteTable(int idTablee)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from Tablee where idTablee="+idTablee+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static String getNameTableByIdTable(int idTablee)
    {
        String name = "";
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Tablee where idTablee="+idTablee+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                while (rs.next())
                    name = rs.getString("nameTablee");
                if (name.isEmpty()==false)
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return name;
    }
    public static ArrayList<Tablee> getListTableByNameTable(String nameTablee)
    {
        ArrayList<Tablee> ds = new ArrayList<>();
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Tablee where nameTablee like '%"+nameTablee+"%'";
            ds.clear();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            while (rs.next())
               ds.add(new Tablee(rs.getInt("idTablee"),rs.getString("nameTablee")));
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return ds;
    }
}
