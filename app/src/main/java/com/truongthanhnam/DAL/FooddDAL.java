package com.truongthanhnam.DAL;

import android.util.Log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class FooddDAL
{
    public static String getNameFooddByIdFoodd(int idFoodd)
    {
        String name = "";
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Foodd where idFoodd="+idFoodd+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                while (rs.next())
                    name = rs.getString("nameFoodd");
                if (name.isEmpty()==false)
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return name;
    }
    public static void addFoodd(int idFoodd,String nameFoodd)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "insert into Foodd values('"+idFoodd+"','"+nameFoodd+"')";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void updateNameFooddByIdFoodd(int idFoodd,String nameFoodd)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "update Foodd set nameFoodd='"+nameFoodd+"' where idFoodd="+idFoodd+"";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                if (rs.next())
                    check1=false;
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static void deleteFooddByIdFoodd(int idFoodd)
    {
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "delete from Foodd where idFoodd="+idFoodd+"";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(str);
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
    }
    public static int autoIdFoodd()
    {
        int id=0;
        try
        {
            Connection con = null;
            boolean check = true;
            while (check)
            {
                ConnectionDAL connect = new ConnectionDAL();
                con = connect.conn();
                if (con!=null)
                    check=false;
            }
            String str = "select * from Foodd";
            boolean check1 = true;
            while (check1)
            {
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str);
                while (rs.next())
                {
                    id = rs.getInt("idFoodd");
                    check1=false;
                }
            }
            con.close();
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return id+1;
    }
}
