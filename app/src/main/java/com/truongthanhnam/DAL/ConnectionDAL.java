package com.truongthanhnam.DAL;

import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by TruongThanh on 27-Sep-17.
 */

public class ConnectionDAL
{
    public static String ip = "";
    public static String classs = "net.sourceforge.jtds.jdbc.Driver";
    public static String db = "";
    public static String username = "";
    public static String pwd = "";
    public Connection conn()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection con = null;
        String connURL = null;
        try
        {
            Class.forName(classs);
            connURL = "jdbc:jtds:sqlserver://"+ip+";"+"databaseName="+db+";user="+username+";password="+pwd+";";
            con = DriverManager.getConnection(connURL);
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return con;
    }
    public static boolean isConnect(String ip,String db,String username,String pwd)
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection con = null;
        String connURL = null;
        try
        {
            Class.forName(classs);
            connURL = "jdbc:jtds:sqlserver://"+ip+";"+"databaseName="+db+";user="+username+";password="+pwd+";";
            con = DriverManager.getConnection(connURL);
            return true;
        }
        catch (Exception ex)
        {
            Log.e("Error:",ex.toString());
        }
        return false;
    }
    public static void getInformation(String str,String database,String user,String password)
    {
        ip = str;
        db = database;
        username = user;
        pwd = password;
    }
}
