package com.truongthanhnam.DAL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

import java.io.ByteArrayOutputStream;

/**
 * Created by TruongThanh on 06-Oct-17.
 */

public class ConvertDAL
{
    public static byte[] imageToByteArray(BitmapDrawable i)
    {
        BitmapDrawable drawable = i;
        Bitmap bmp = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] b = stream.toByteArray();
        return b;
    }
    public static Bitmap byteArrayToBitMap(byte[] arr)
    {
        Bitmap bitmap = BitmapFactory.decodeByteArray(arr,0,arr.length);
        return bitmap;
    }
}
